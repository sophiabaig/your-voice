# Getting Started with Starting Up Backend Server

### `cd back-end`
### `python3 -m venv venv`
### `. venv/bin/activate`
### `pip install -r requirements.txt`
### `flask run`