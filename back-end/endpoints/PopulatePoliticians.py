from flask import Blueprint
from services import db, app
import os
import json

politician = Blueprint("politicians",__name__)
JSON_POLITICIANS = os.path.join(os.path.dirname(__file__), "../data/congressMembers.json")

class card(db.Model):
    __tablename__ = 'politicians'

    id = db.Column(db.Integer , primary_key=True)
    name = db.Column(db.String())
    position = db.Column(db.String())
    age = db.Column(db.String())
    party = db.Column(db.String())
    twitter = db.Column(db.String())
    facebook = db.Column(db.String())
    state = db.Column(db.String())
    terms = db.Column(db.String())
    district = db.Column(db.String())
    link = db.Column(db.String())
    image = db.Column(db.String())
    phone = db.Column(db.String())
    address = db.Column(db.String())
    gender = db.Column(db.String())
    news_id = db.Column(db.String())
    committee_id = db.Column(db.String())
    not_filled = db.Column(db.Integer)
    
    def serialize(self):
        serialObj = dict({
            "id": self.id,
            "name": self.name,
            "position": self.position,
            "age": self.age,
            "party": self.party,
            "twitter": self.twitter,
            "facebook":self.facebook,
            "state": self.state,
            "terms": self.terms,
            "district": self.district,
            "link": self.link,
            "image": self.image,
            "phone": self.phone,
            "address":self.address,
            "gender": self.gender,
            "news_id": self.news_id,
            "committee_id": self.committee_id,
            "not_filled":self.not_filled,
        })
        return serialObj


def populate_politicians():
    with open(JSON_POLITICIANS) as d:
        data = json.load(d)
        for i in range(0, len(data)):
            try:
                db_row = dict({ 
                    "name" : data[i]["name"],
                    "party": data[i]["party"],
                    "position": data[i]["position"],
                    "state": data[i]["state"],
                    "twitter": data[i]["twitter"],
                    "facebook":data[i]["facebook"],
                    "link" : data[i]["url"],
                    "image":data[i]["photoUrl"],
                    "phone":data[i]["phone"],
                    "address":data[i]["address"],
                    "age": get_age(data[i]["dob"]),
                    "terms":data[i]["seniority"],
                    "gender": get_gender(data[i]["gender"]),
                    "district": data[i]["district"]
                })
                with app.app_context():
                    temp = card(**db_row)
                    db.session.add(temp)
                    db.session.commit()
            except:
                print("FAILED")
                continue


def get_age(date):
    date = date.split("-")
    this_year = 2023
    this_month = 3
    if int(date[1]) < this_month:
        age = this_year - int(date[0]) - 1
    else:
        age = this_year - int(date[0])
    return age

def get_gender(gender):
    if gender == "F":
        return "Female"
    else:
        return "Male"

def fix_rep_typo():
    with app.app_context():
        politicians = card.query.filter_by(position = "House Represntative").all()
        for politician in politicians:
            politician.position = "House Representative"
            db.session.commit()

def populate_rep_images():
    with app.app_context():
        with open(JSON_POLITICIANS) as d:
            data = json.load(d)
            for i in range(0, len(data)):
                politician = card.query.filter_by(name = data[i]["name"]).first()
                politician.image = data[i]["photoUrl"]
                db.session.commit()