import json
import os
from services import app, db
from endpoints.PopulatePoliticians import card as Politician, politician
from endpoints.PopulateCommittees import card as Committee, committees
from endpoints.PopulateNews import card as News, news

JSON_COMMITTEE_MEMS = os.path.join(os.path.dirname(__file__), "./data/committeeMembers.json")
JSON_POLITICIANS = os.path.join(os.path.dirname(__file__), "../data/congressMembers.json")

def relate_sub_committees():
    json_file = JSON_COMMITTEE_MEMS
    with app.app_context():
        with open(json_file) as dt:
            data = json.load(dt)
            for committee_name in data:
                committee = Committee.query.filter(Committee.sub_id.like(committee_name)).first()
                if type(committee) == Committee:
                    for mem in range(0, len(data[committee_name])):
                        name = data[committee_name][mem]["name"]
                        politician = Politician.query.filter_by(name = name).first()
                        if type(politician) == Politician:
                            politician.committee_id = committee.id
                            if type(committee.senator_id) != type(None):
                                committee.senator_id = committee.senator_id + " " + str(politician.id)
                            else:
                                committee.senator_id = str(politician.id)
                            db.session.commit()

def relate_main_committees():
    json_file = JSON_COMMITTEE_MEMS
    with app.app_context():
        with open(json_file) as dt:
            data = json.load(dt)
            for committee_name in data:
                committees = Committee.query.filter(Committee.main_id.like(committee_name)).filter(Committee.senator_id == None).all()
                for mem in range(0, len(data[committee_name])):
                    name = data[committee_name][mem]["name"]
                    politician = Politician.query.filter_by(name = name).first()
                    if type(politician) == Politician:
                        for committee in committees:
                            if type(politician.committee_id) != type(None):
                                politician.committee_id = politician.committee_id + " " + str(committee.id)
                            else:
                                politician.committee_id = str(committee.id)
                            if type(committee.senator_id) != type(None):
                                committee.senator_id = committee.senator_id + " " + str(politician.id)
                            else:
                                committee.senator_id = str(politician.id)
                            db.session.commit()

def relate_news():
    with app.app_context():
        for news in News.query.all():  # all() is extra        
            politician = Politician.query.filter_by(name = news.keyword).first()
            if type(politician) == Politician:
                news.senator_id = politician.id
                news.committee_id = politician.committee_id
                if type(politician.news_id) != type(None):
                    politician.news_id = politician.news_id + " " + str(news.id)
                else:
                    politician.news_id = str(news.id)
            committee = Committee.query.filter_by(name = news.keyword).first()
            if type(committee) == Committee:
                news.committee_id = committee.id
                news.senator_id = committee.senator_id
                if type(committee.news_id) != type(None):
                    committee.news_id = committee.news_id + " " + str(news.id)
                else:
                    committee.news_id = str(news.id)
        db.session.commit()

def relate_news_empty():
    with app.app_context():
        for news in News.query.all():  # all() is extra        
            politician = Politician.query.filter_by(name = news.keyword).first()
            if type(politician) == Politician:
                news.senator_id = politician.id
                news.committee_id = politician.committee_id
                if type(politician.news_id) != type(None):
                    politician.news_id = politician.news_id + " " + str(news.id)
                else:
                    politician.news_id = str(news.id)
            committee = Committee.query.filter_by(name = news.keyword).first()
            if type(committee) == Committee:
                news.committee_id = committee.id
                news.senator_id = committee.senator_id
                if type(committee.news_id) != type(None):
                    committee.news_id = committee.news_id + " " + str(news.id)
                else:
                    committee.news_id = str(news.id)
        db.session.commit()

def relate_news_empty_committees():
    with app.app_context():
        committees = Committee.query.filter(Committee.news_id == None).all()
        for committee in committees: 
            news = News.query.filter(News.keyword == committee.main_committee).all()
            ids = ""
            for new in news:
                ids += str(new.id) + " "
                new.committee_id = committee.id
                new.senator_id = committee.senator_id
            if ids:
                committee.news_id = ids
        db.session.commit()

def relate_empty_news_committees():
    with app.app_context():
        news = News.query.filter(News.senator_id == None).all()
        for new in news: 
            committee = Committee.query.filter(Committee.name == new.keyword).first()
            if committee != None:
                new.senator_id = committee.senator_id
        db.session.commit()
        
def relate_main_committees_only():
    json_file = JSON_COMMITTEE_MEMS
    with app.app_context():
        with open(json_file) as dt:
            data = json.load(dt)
            for committee_name in data:
                committees = Committee.query.filter(Committee.senator_id == None).filter(Committee.main_id.like(committee_name)).all()
                if committees:
                    for mem in range(0, len(data[committee_name])):
                        name = data[committee_name][mem]["name"]
                        politician = Politician.query.filter_by(name = name).first()
                        if type(politician) == Politician:
                            for committee in committees:
                                if type(politician.committee_id) != type(None):
                                    politician.committee_id = politician.committee_id + " " + str(committee.id)
                                else:
                                    politician.committee_id = str(committee.id)
                                if type(committee.senator_id) != type(None):
                                    committee.senator_id = committee.senator_id + " " + str(politician.id)
                                else:
                                    committee.senator_id = str(politician.id)
                                db.session.commit()




