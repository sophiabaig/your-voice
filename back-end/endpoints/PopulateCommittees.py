from flask import Blueprint
from services import db, app
import os
import json

committees = Blueprint("committees",__name__)
JSON_COMMITTEES = os.path.join(os.path.dirname(__file__), "../data/committees.json")

class card(db.Model):
    __tablename__ = 'committee'

    id = db.Column(db.Integer , primary_key=True)
    main_id = db.Column(db.String())
    sub_id = db.Column(db.String())
    congress = db.Column(db.String())
    chamber = db.Column(db.String())
    main_committee = db.Column(db.String())
    chair = db.Column(db.String())
    chair_party = db.Column(db.String())
    chair_state = db.Column(db.String())
    name = db.Column(db.String())
    senator_id = db.Column(db.String())
    news_id =  db.Column(db.String())
    twitter = db.Column(db.String())
    image = db.Column(db.String())
    not_filled = db.Column(db.Integer)

    
    def serialize(self):
        serialObj = dict({
            "id": self.id,
            "main_id": self.main_id,
            "sub_id": self.sub_id,
            "congress": self.congress,
            "chamber": self.chamber,
            "main_committee": self.main_committee,
            "chair": self.chair,
            "chair_party": self.chair_party,
            "chair_state": self.chair_state,
            "name": self.name,
            "senator_id":self.senator_id,
            "news_id": self.news_id,
            "twitter":self.twitter,
            "image":self.image,
            "not_filled":self.not_filled,
        })
        return serialObj


def populate_committees():
    with open(JSON_COMMITTEES) as d:
        data = json.load(d)
        print("main")
        for i in range(0, len(data)):
            r = 0
            for c in range(0, len(data[i]["results"][r]["committees"])):
                for s in range(0, len(data[i]["results"][r]["committees"][c]["subcommittees"])):
                    try:
                        db_row = dict({ 
                            "main_id": data[i]["results"][r]["committees"][c]["id"],
                            "sub_id": data[i]["results"][r]["committees"][c]["subcommittees"][s]["id"],
                            "congress": data[i]["results"][r]["congress"],
                            "chamber": data[i]["results"][r]["chamber"],
                            "main_committee": data[i]["results"][r]["committees"][c]["name"],
                            "chair": data[i]["results"][r]["committees"][c]["chair"],
                            "chair_party": get_party(data[i]["results"][r]["committees"][c]["chair_party"]),
                            "chair_state": data[i]["results"][r]["committees"][c]["chair_state"],
                            "name": data[i]["results"][r]["committees"][c]["subcommittees"][s]["name"],
                            "twitter": data[i]["results"][r]["committees"][c]["twitter_handle"],
                            "image":data[i]["results"][r]["committees"][c]["logo_url"]
                        })
                        with app.app_context():
                            temp = card(**db_row)
                            db.session.add(temp)
                            db.session.commit()
                    except:
                        print("FAILED")
                        continue

def populate_main_committees():
    with open(JSON_COMMITTEES) as d:
        data = json.load(d)
        print("main")
        for i in range(0, len(data)):
            r = 0
            for c in range(0, len(data[i]["results"][r]["committees"])):
                try:
                    db_row = dict({ 
                        "main_id": data[i]["results"][r]["committees"][c]["id"],
                        "congress": data[i]["results"][r]["congress"],
                        "chamber": data[i]["results"][r]["chamber"],
                        "chair": data[i]["results"][r]["committees"][c]["chair"],
                        "chair_party": get_party(data[i]["results"][r]["committees"][c]["chair_party"]),
                        "chair_state": data[i]["results"][r]["committees"][c]["chair_state"],
                        "name":  data[i]["results"][r]["committees"][c]["name"]
                    })
                    with app.app_context():
                        temp = card(**db_row)
                        db.session.add(temp)
                        db.session.commit()
                except:
                    print("FAILED")
                    continue

def get_party(party):
    if party == "D":
        return "Democratic Party"
    else:
        return "Republican Party"
