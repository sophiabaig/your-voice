import json, os
statesDict = {"AL" : "Alabama", "AK": "Alaska", "AZ" : "Arizona", "AR" : "Arkansas",
                    "CA" : "California", "CO" : "Colorado", "CT" : "Connecticut", "DE" : "Delaware",
                    "FL" : "Florida", "GA" : "Georgia", "HI" : "Hawaii", "ID" : "Idaho",
                    "IL" : "Illinois", "IN" : "Indiana", "IA" : "Iowa", "KS": "Kansas",
                    "KY" : "Kentucky", "LA" : "Louisiana", "ME" : "Maine", "MD" : "Maryland",
                    "MA" : "Massachusetts", "MI" : "Michigan", "MN" : "Minnesota", "MS" : "Mississippi",
                    "MO" : "Missouri", "MT" : "Montana", "NE" : "Nebraska", "NV" : "Nevada",
                    "NH" : "New Hampshire", "NJ" : "New Jersey", "NM" : "New Mexico", "NY" : "New York",
                    "NC" : "North Carolina", "ND" : "North Dakota", "OH" : "Ohio", "OK" : "Oklahoma", 
                    "OR" : "Oregon", "PA" : "Pennsylvania", "RI" : "Rhode Island", "SC" : "South Carolina", 
                    "SD" : "South Dakota", "TN" : "Tennessee", "TX" : "Texas", "UT" : "Utah", 
                    "VT" : "Vermont", "VA" : "Virginia", "WA" : "Washington", "WV" : "West Virginia", 
                    "WI" : "Wisconsin", "WY" : "Wyoming"}

class Congress_Member():
    def __init__(self, id, name, party, position, state, twitter, facebook, url, photoUrl, phone, address, dob, seniority, gender, district):
        self.id = id
        self.name = name
        self.party = party
        self.position = position
        self.state = state
        self.twitter = twitter
        self.facebook = facebook
        self.url = url
        self.photoUrl = photoUrl
        self.phone = phone
        self.address = address
        self.dob = dob
        self.seniority = seniority
        self.gender = gender
        self.district = district

def build_dict():
    members = []
    id = 0

    with open ("../data/houseMembers.json") as f:
        houseData = json.load(f)

    with open ("../data/senateMembers.json") as ff:
        senateData = json.load(ff)

    with open ("../data/senators.json") as fff:
        senators = json.load(fff)

   
    # House members    
    for member in houseData[0]["results"][0]["members"]:
        if member["title"] != "Representative":
            continue

        name = ""
        if member["middle_name"] is not None:
            name = member["first_name"] + " " + member["middle_name"] + " " + member["last_name"]
        else:
            name = member["first_name"] + " " + member["last_name"]
        
        party = ""
        if member["party"] == "D":
            party = "Democratic Party"
        elif member["party"] == "R":
            party = "Republican Party"
        else:
            party = "Unaffiliated"

        position = "House Represntative"

        state = statesDict[member["state"]]

        twitter = member["twitter_account"]
        facebook = member["facebook_account"]

        url = member["url"]

        phone = member["phone"]

        photoUrl = ""

        address = member["office"]

        dob = member["date_of_birth"]

        seniority = member["seniority"]

        gender = member["gender"]

        district = member["district"]

        memberObj = Congress_Member(id, name, party, position, state, twitter, facebook, url, photoUrl, phone, address, dob, seniority, gender, district)
        members.append(memberObj.__dict__)
        id += 1

    # Senate memebers
    for member in senateData[0]["results"][0]["members"]:

        name = ""
        if member["middle_name"] is not None:
            name = member["first_name"] + " " + member["middle_name"] + " " + member["last_name"]
        else:
            name = member["first_name"] + " " + member["last_name"]
        
        party = ""
        if member["party"] == "D":
            party = "Democratic Party"
        elif member["party"] == "R":
            party = "Republican Party"
        else:
            party = "Unaffiliated"

        position = "U.S. Senator"

        state = statesDict[member["state"]]

        twitter = member["twitter_account"]
        facebook = member["facebook_account"]

        url = member["url"]

        photoUrl = ""

        phone = member["phone"]

        address = member["office"]

        dob = member["date_of_birth"]

        seniority = member["seniority"]

        gender = member["gender"]

        district = ""

        # Get the data from senators.json to have the most up to date info
        sen_dict = {} # Empty dict reference
        for senator in senators :
            if name == senator["name"] :
                # print("found senator") # 7 are new
                sen_dict = senator
                break
        
        # Updates twitter and facebook info
        try:
            for social in sen_dict["socials"] :
                if social["type"] == "Facebook" :
                    facebook = social["id"]
                elif social ["type"] == "Twitter" :
                    twitter = social["id"]
        except KeyError:
            pass

        try:
            url = sen_dict["urls"][0]
        except KeyError:
            pass
        
        try:
            photoUrl = sen_dict["photoUrl"]
        except KeyError:
            pass

        try:
            phone = sen_dict["phones"][0]
        except KeyError:
            pass


        memberObj = Congress_Member(id, name, party, position, state, twitter, facebook, url, photoUrl, phone, address, dob, seniority, gender, district)
        members.append(memberObj.__dict__)
        id += 1
    
    create_json_file("../data/test1.json", members)
    print("done")

def create_json_file(file_path, data = []):
    """
    creates a json file with a list at the specified path if it does not exist
    """
    JSON_INDENT = 4
    file_exists = os.path.exists(file_path)
    if(not file_exists):
        with open(file_path, 'w') as file:
            json.dump(data, file, indent = JSON_INDENT)

if __name__ == "__main__":
    build_dict()