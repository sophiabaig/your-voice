import json
import requests
import config

def get_committees():
    KEY = config.api_key_committees
    URL = "https://api.propublica.org/congress/v1/116/"
    URL_EXND = "/committees.json"
    PARAMS = ["house", "senate", "joint"]

    committe_list = []

    for chamber in PARAMS:
        call_response = requests.get(URL + chamber + URL_EXND, headers={'X-API-Key': config.api_key_committees})
        response_dict = call_response.json()
        committe_list.append(response_dict)
    
    print("Generating Data")
    generateJSONData("../data/committeeData.json", committe_list)
    print("Done")

def generateJSONData(file_path, data):

    with open(file_path, 'w') as file_descriptor:
        print("Writing to file")
        json.dump(data, file_descriptor, indent=2)