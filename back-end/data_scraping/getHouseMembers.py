import json
import requests

def get_members():
    KEY = "bSYjIqHcb3G2FmpA7s4z56u1J7G06P3Rm9mnNwOX"
    URL = "https://api.propublica.org/congress/v1/117/senate/"
    URL_EXND = "members.json"

    members = []

    call_response = requests.get(URL + URL_EXND, headers={'X-API-Key': 'bSYjIqHcb3G2FmpA7s4z56u1J7G06P3Rm9mnNwOX'})
    response_dict = call_response.json()
    members.append(response_dict)

    print("Generating Data")
    generateJSONData("../data/senateMembers.json", members)
    print("Done")

def generateJSONData(file_path, data):

    with open(file_path, 'w') as file_descriptor:
        print("Writing to file")
        json.dump(data, file_descriptor, indent=2)      
