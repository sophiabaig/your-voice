import json, os
import requests

class Article(): 
    def __init__(self, id, keyword, source, author, title, description, url, urlToImage, publishedDate, content):
        self.id = id
        self.keyword = keyword
        self.source = source
        self.author = author
        self.title = title
        self.description = description
        self.url = url
        self.urlToImage = urlToImage
        self.publishedDate = publishedDate
        self.content = content

def get_articles():
    URL = "https://newsapi.org/v2/everything?q="
    KEY = "&pageSize=10&searchIn=title,description&apiKey=d3edcdfa7d494973b397823a63f00d6b"
    TOPIC_LIST = [
    "Tammy Baldwin",
    "John Barrasso",
    "Michael F. Bennet",
    "Marsha Blackburn",
    "Richard Blumenthal",
    "Eric Schmitt",
    "Cory A. Booker",
    "John Boozman",
    "Mike Braun",
    "Sherrod Brown",
    "Ted Budd",
    "Maria Cantwell",
    "Shelley Moore Capito",
    "Benjamin L. Cardin",
    "Thomas R. Carper",
    "Bob Casey",
    "Bill Cassidy",
    "Susan M. Collins",
    "Christopher A. Coons",
    "John Cornyn",
    "Catherine Cortez Masto",
    "Tom Cotton",
    "Kevin Cramer",
    "Michael D. Crapo",
    "Ted Cruz",
    "Steve Daines",
    "Tammy Duckworth",
    "Richard J. Durbin",
    "Joni Ernst",
    "Dianne Feinstein",
    "Deb Fischer",
    "Kirsten E. Gillibrand",
    "Lindsey Graham",
    "Charles E. Grassley",
    "Bill Hagerty",
    "Margaret Wood Hassan",
    "Joshua Hawley",
    "Martin Heinrich",
    "John W. Hickenlooper",
    "Mazie K. Hirono",
    "John Hoeven",
    "Cindy Hyde-Smith",
    "Markwayne Mullin",
    "Ron Johnson",
    "Tim Kaine",
    "Mark Kelly",
    "John Kennedy",
    "Angus S. King Jr.",
    "Amy Klobuchar",
    "James Lankford",
    "Peter Welch",
    "Mike Lee",
    "Ben Ray Luj\u00e1n",
    "Cynthia M. Lummis",
    "Joe Manchin",
    "Edward J. Markey",
    "Roger Marshall",
    "Mitch McConnell",
    "Robert Menendez",
    "Jeff Merkley",
    "Jerry Moran",
    "Lisa Murkowski",
    "Christopher S. Murphy",
    "Patty Murray",
    "Jon Ossoff",
    "Alex Padilla",
    "Rand Paul",
    "Gary Peters",
    "JD Vance",
    "Jack Reed",
    "Jim E. Risch",
    "Mitt Romney",
    "Jacky Rosen",
    "Mike Rounds",
    "Marco Rubio",
    "Bernard Sanders",
    "Pete Ricketts",
    "Brian Schatz",
    "Charles E. Schumer",
    "Tim Scott",
    "Rick Scott",
    "Jeanne Shaheen",
    "Katie Boyd Britt",
    "Kyrsten Sinema",
    "Tina Smith",
    "Debbie Stabenow",
    "Dan Sullivan",
    "Jon Tester",
    "John Thune",
    "Thom Tillis",
    "John Fetterman",
    "Tommy Tuberville",
    "Chris Van Hollen",
    "Mark R. Warner",
    "Raphael Warnock",
    "Elizabeth Warren",
    "Sheldon Whitehouse",
    "Roger Wicker",
    "Ron Wyden",
    "Todd Young"
]
    KEY1 = "e4cac6da08f540c78de5e7a72205e8d0"
    KEY2 = "39c089dea7c94fd0bc11dd88c3a18555"
    KEY3 = "75b77653aacb456ea1e1bbb10440b6a5"
    KEY4 = "776fe361423645a8a630d26e4798c223"
    KEY5 = "6bcc6c4d54ad4a99b9ff4f8ff8655e81"
    KEY6 = "d3edcdfa7d494973b397823a63f00d6b"
    articles_list = []


    for keyword in TOPIC_LIST:
        call_reponse = requests.get(URL + "\"" + keyword + "\"" + KEY)
        response_dict = call_reponse.json()
        response_dict["keyword"] = keyword
        articles_list.append(response_dict)
    
    print("Generating data")
    generateJSONData("../data/test10.json", articles_list)
    print("Done")


def generateJSONData(file_path, data):

    with open(file_path, 'w') as file_descriptor:
        json.dump(data, file_descriptor, indent=4)

if __name__ == "__main__":
    get_articles()

def generateArray():
    print("generating array")
    house_members = []

    with open("../data/congressMembers.json") as file_descriptor:
        politicians = json.load(file_descriptor)
    
    print("loaded file")

    for politician in politicians:
        if(politician["position"] == "U.S. Senator"):
            house_members.append(politician["name"])
    
    print("generated array")
    print("writing to file . . .")
    generateJSONData("../data/test9.json", house_members)