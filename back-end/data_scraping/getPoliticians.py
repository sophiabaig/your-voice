import json, os
import requests
from requests.auth import HTTPBasicAuth
import config


class Offical():
    def __init__(self, id, name, position, state, role, address, party, phones, urls, photoUrl, socials):
        self.id = id
        self.name = name
        self.position = position
        self.state = state
        self.role = role
        self.address = address
        self.party = party
        self.phones = phones
        self.urls = urls
        self.photoUrl = photoUrl
        self.socials = socials

def get_politicians() :

    apiKey = config.api_key_politicians
    URL = "https://www.googleapis.com/civicinfo/v2/representatives/ocd-division"

    #OcdId dictionary for searching
    statesDict = { "%2Fcountry%3Aus": "", "%2Fcountry%3Aus%2Fstate%3Aal" : "Alabama", "%2Fcountry%3Aus%2Fstate%3Aak": "Alaska", "%2Fcountry%3Aus%2Fstate%3Aaz" : "Arizona", "%2Fcountry%3Aus%2Fstate%3Aar" : "Arkansas",
                    "%2Fcountry%3Aus%2Fstate%3Aca" : "California", "%2Fcountry%3Aus%2Fstate%3Aco" : "Colorado", "%2Fcountry%3Aus%2Fstate%3Act" : "Connecticut", "%2Fcountry%3Aus%2Fstate%3Ade" : "Delaware",
                    "%2Fcountry%3Aus%2Fstate%3Afl" : "Florida", "%2Fcountry%3Aus%2Fstate%3Aga" : "Georgia", "%2Fcountry%3Aus%2Fstate%3Ahi" : "Hawaii", "%2Fcountry%3Aus%2Fstate%3Aid" : "Idaho",
                    "%2Fcountry%3Aus%2Fstate%3Ail" : "Illinois", "%2Fcountry%3Aus%2Fstate%3Ain" : "Indiana", "%2Fcountry%3Aus%2Fstate%3Aia" : "Iowa", "%2Fcountry%3Aus%2Fstate%3Aks": "Kansas",
                    "%2Fcountry%3Aus%2Fstate%3Aky" : "Kentucky", "%2Fcountry%3Aus%2Fstate%3Ala" : "Louisiana", "%2Fcountry%3Aus%2Fstate%3Ame" : "Maine", "%2Fcountry%3Aus%2Fstate%3Amd" : "Maryland",
                    "%2Fcountry%3Aus%2Fstate%3Ama" : "Massachusetts", "%2Fcountry%3Aus%2Fstate%3Ami" : "Michigan", "%2Fcountry%3Aus%2Fstate%3Amn" : "Minnesota", "%2Fcountry%3Aus%2Fstate%3Ams" : "Mississippi",
                    "%2Fcountry%3Aus%2Fstate%3Amo" : "Missouri", "%2Fcountry%3Aus%2Fstate%3Amt" : "Montana", "%2Fcountry%3Aus%2Fstate%3Ane" : "Nebraska", "%2Fcountry%3Aus%2Fstate%3Anv" : "Nevada",
                    "%2Fcountry%3Aus%2Fstate%3Anh" : "New Hampshire", "%2Fcountry%3Aus%2Fstate%3Anj" : "New Jersey", "%2Fcountry%3Aus%2Fstate%3Anm" : "New Mexico", "%2Fcountry%3Aus%2Fstate%3Any" : "New York",
                    "%2Fcountry%3Aus%2Fstate%3Anc" : "North Carolina", "%2Fcountry%3Aus%2Fstate%3And" : "North Dakota", "%2Fcountry%3Aus%2Fstate%3Aoh" : "Ohio", "%2Fcountry%3Aus%2Fstate%3Aok" : "Oklahoma", 
                    "%2Fcountry%3Aus%2Fstate%3Aor" : "Oregon", "%2Fcountry%3Aus%2Fstate%3Apa" : "Pennsylvania", "%2Fcountry%3Aus%2Fstate%3Ari" : "Rhode Island", "%2Fcountry%3Aus%2Fstate%3Asc" : "South Carolina", 
                    "%2Fcountry%3Aus%2Fstate%3Asd" : "South Dakota", "%2Fcountry%3Aus%2Fstate%3Atn" : "Tennessee", "%2Fcountry%3Aus%2Fstate%3Atx" : "Texas", "%2Fcountry%3Aus%2Fstate%3Aut" : "Utah", 
                    "%2Fcountry%3Aus%2Fstate%3Avt" : "Vermont", "%2Fcountry%3Aus%2Fstate%3Ava" : "Virginia", "%2Fcountry%3Aus%2Fstate%3Awa" : "Washington", "%2Fcountry%3Aus%2Fstate%3Awv" : "West Virginia", 
                    "%2Fcountry%3Aus%2Fstate%3Awi" : "Wisconsin", "%2Fcountry%3Aus%2Fstate%3Awy" : "Wyoming" }

    #start of to be JSON file
    politicians = []
    id = 0

    for key in statesDict:

        request = requests.get(URL + key +"?key=" + apiKey)
        data = json.loads(request.text)

        for office in data["offices"] :
            position = office["name"]
            roles = office["roles"]
            officialIndecies = office["officialIndices"]
            for ind in officialIndecies:
                official = data["officials"][ind] 
                name = official["name"]
                off_pos = position
                role = roles

                # Series of try blocks since the following information is not guarenteed
                address = []
                try:
                    address = official["address"]
                except KeyError:
                    pass

                party = ""
                try:
                    party = party = official["party"]
                except KeyError:
                    pass

                phones = []
                try:
                    phones = official["phones"]
                except KeyError:
                    pass

                urls = []
                try:
                    urls = official["urls"]
                except KeyError:
                    pass
                
                photoUrl = ""
                try:
                    photoUrl = official["photoUrl"]
                except KeyError:
                    pass
                
                socials = []
                try:
                    socials = official["channels"]
                except KeyError:
                    pass
                
                officialObject = Offical(id, name, off_pos, statesDict[key], role, address, party, phones, urls, photoUrl, socials)
                politicians.append(officialObject.__dict__)
                id += 1

    create_json_file("../data/politicians.json", politicians)
    print("done")

def create_json_file(file_path, data = []):
    """
    creates a json file with a list at the specified path if it does not exist
    """
    JSON_INDENT = 4
    file_exists = os.path.exists(file_path)
    if(not file_exists):
        with open(file_path, 'w') as file:
            json.dump(data, file, indent = JSON_INDENT)


if __name__ == "__main__":
    get_politicians()
