import time
import requests
import json, os

def update_image_url() :

    YOUR_API_KEY = 'b4cf7246cb784a16b61c90183073456c'
    headers = {'Ocp-Apim-Subscription-Key': YOUR_API_KEY}

    with open ("../data/congressMembers.json") as f:
        data = json.load(f)

    new_members = []
    id = 0

    for member in data :
        if member["position"] == "House Represntative" :
    
            url = f'https://api.bing.microsoft.com/v7.0/images/search?q={member["name"]}&count=1'
            response = requests.get(url, headers=headers).json()
            image_url = response['value'][0]['contentUrl']
            
            member["photoUrl"] = image_url
            id += 1

            new_members.append(member)
            time.sleep(0.5) # Sleep for 0.5 seconds to make sure we stay within the '3 searches per second' limit
        new_members.append(member)
    
    create_json_file("../data/test.json", new_members)
    print("done")



def create_json_file(file_path, data = []):
    """
    creates a json file with a list at the specified path if it does not exist
    """
    JSON_INDENT = 4
    file_exists = os.path.exists(file_path)
    if(not file_exists):
        with open(file_path, 'w') as file:
            json.dump(data, file, indent = JSON_INDENT)


if __name__ == "__main__":
    update_image_url()