# Code citation: Team Geo Jobs at https://gitlab.com/sarthaksirotiya/cs373-idb
import app
import unittest

class Tests(unittest.TestCase):
    def setUp(self):
        app.app.config["TESTING"] = True
        self.client = app.app.test_client()

    def testGetAllPoliticians(self):
        print("test get all senators . . .")
        with self.client:
            response = self.client.get("/get/politicians")
            self.assertEqual(response.status_code, 200)
            data = response.json["data"]
            #This value is 9 because that is our default size
            self.assertEqual(len(data["ids"]), 549)
            print("SUCCESS !")
    
    def testGetAllCommittees(self):
        print("testing get all committees . . .")
        with self.client:
            response = self.client.get("/get/committees")
            self.assertEqual(response.status_code, 200)
            data = response.json["data"]
            #This vaue is 9 because that is our default page size
            self.assertEqual(len(data["ids"]), 249)
            print("SUCCESS !")

    def testGetNews(self):
        print("testing get all news . . .")
        with self.client:
            response = self.client.get("/get/news")
            self.assertEqual(response.status_code, 200)
            data = response.json["data"]
            #This vaue is 9 because that is our default page size
            self.assertEqual(len(data["ids"]), 6216)
            print("SUCCESS !")
    
    def testGetNewsInstance(self):
        print("testing news instance . . .")
        with self.client:
            #Ttesting news instance of id = 250
            response = self.client.get("/get/news/250")
            self.assertEqual(response.status_code, 200)
            
            author = response.json["news"]["author"]
            self.assertEqual(author, "Conover Kennard")
            
            date = response.json["news"]["date"]
            self.assertEqual(date, "2023-03-12T16:56:00Z")

            committee_id = response.json["news"]["committee_id"]
            self.assertEqual(committee_id, '138 193 194 195 196 197 198 199 200')

            id = response.json["news"]["id"]
            self.assertEqual(id, 250)

            keyword = response.json["news"]["keyword"]
            self.assertEqual(keyword, "John Kennedy")

            publisher = response.json["news"]["publisher"]
            self.assertEqual(publisher, 'Crooksandliars.com')

            senator_id = response.json["news"]["senator_id"]
            self.assertEqual(senator_id, '496')

            title = response.json["news"]["title"]
            self.assertEqual(title, 'Sen. John Kennedy Chomping At The Bit To Cut Social Security')

            print("SUCCESS !")
        
    def testCommitteeInstance(self):
        print("Testing committee instance")
        with self.client:
            #Testing Committee Instance with id = 37
            response = self.client.get("/get/committees/37")
            self.assertEqual(response.status_code, 200)

            chair = response.json["news"]["chair"]
            self.assertEqual(chair, "Frank Pallone")
    
            chair_party = response.json["news"]["chair_party"]
            self.assertEqual(chair_party, "Democratic Party")

            chair_state = response.json["news"]["chair_state"]
            self.assertEqual(chair_state, "NJ")

            chamber = response.json["news"]["chamber"]
            self.assertEqual(chamber, "House")

            congress = response.json["news"]["congress"]
            self.assertEqual(congress, '116')

            main_committee = response.json["news"]["main_committee"]
            self.assertEqual(main_committee, 'Committee on Energy and Commerce')

            main_id = response.json["news"]["main_id"]
            self.assertEqual(main_id, 'HSIF')

            name = response.json["news"]["name"]
            self.assertEqual(name, "Environment and Climate Change")

            twitter = response.json["news"]["twitter"]
            self.assertEqual(twitter, 'HouseCommerce')

            print("SUCCESS !")

    def testSenatorInstance(self):
        print("Testing Senator Instance")
        with self.client:
            response = self.client.get("/get/politicians/40")
            self.assertEqual(response.status_code, 200)

            name = response.json["news"]["name"]
            self.assertEqual(name, "Anthony Brown")

            address = response.json["news"]["address"]
            self.assertEqual(address, "1323 Longworth House Office Building")

            committee_id = response.json["news"]["committee_id"]
            self.assertEqual(committee_id, None)

            facebook = response.json["news"]["facebook"]
            self.assertEqual(facebook, "RepAnthonyBrown")

            id = response.json["news"]["id"]
            self.assertEqual(id, 40)

            party = response.json["news"]["party"]
            self.assertEqual(party, 'Democratic Party')

            phone = response.json["news"]["phone"]
            self.assertEqual(phone, '202-225-8699')

            position = response.json["news"]["position"]
            self.assertEqual(position, 'House Representative')

            state = response.json["news"]["state"]
            self.assertEqual(state, 'Maryland')

            twitter = response.json["news"]["twitter"]
            self.assertEqual(twitter, 'RepAnthonyBrown')

            print("SUCCESS !")

    def test_search_news(self):
        print("testing news search . . .")
        with self.client:
            response = self.client.get("/get/news")
            self.assertEqual(response.status_code, 200)

            total = response.json["total"]
            self.assertEqual(total, 6216)

            author = response.json["news"][0]["author"]
            self.assertEqual(author, "Linda Pentz Gunter")

            committee_id = response.json["news"][0]["committee_id"]
            self.assertEqual(committee_id, '222')

            date = response.json["news"][0]["date"]
            self.assertEqual(date, '2023-02-21T06:56:27Z')

            index = 1
            while(True):
                assert response.json["news"][index]
                assert response.json["news"][index]["author"]
                assert response.json["news"][index]["committee_id"]
                assert response.json["news"][index]["date"]
                assert response.json["news"][index]["content"]
                assert response.json["news"][index]["id"]
                assert response.json["news"][index]["image"]
                assert response.json["news"][index]["keyword"]
                assert response.json["news"][index]["link"]
                assert response.json["news"][index]["month"]
                assert response.json["news"][index]["publisher"]
                assert response.json["news"][index]["title"]
                index = index + 1
                if(index == 9):
                    break

            print("SUCCESS !")

    def test_search_senators(self):
        print("testing search sentors . . .")
        with self.client:
            response =self.client.get("/get/politicians")
            self.assertEqual(response.status_code, 200)

            address = response.json["news"][0]["address"]
            self.assertEqual(address, '300 Cannon House Office Building')

            age = response.json["news"][0]["age"]
            self.assertEqual(age, 62)

            gender = response.json["news"][0]["gender"]
            self.assertEqual(gender, 'Male')

            name = response.json["news"][0]["name"]
            self.assertEqual(name, 'Brad Schneider')

            state = response.json["news"][0]["state"]
            self.assertEqual(state, 'Illinois')

            index = 1
            while(True):
                assert response.json["news"][index]
                assert response.json["news"][index]["age"]
                assert response.json["news"][index]["gender"]
                assert response.json["news"][index]["name"]
                assert response.json["news"][index]["id"]
                assert response.json["news"][index]["state"]
                assert response.json["news"][index]["terms"]
                assert response.json["news"][index]["party"]
                assert response.json["news"][index]["position"]

                index = index + 1
                if(index == 9):
                    break
            
            print("SUCCESS !")

    def test_search_committees(self):
        print("testing search committees . . .")
        with self.client:
            response = self.client.get('/get/committees')
            self.assertTrue(response.status_code, 200)

            chair = response.json["news"][0]["chair"]
            self.assertEquals(chair, 'Mark Takano')

            chair_party = response.json["news"][0]["chair_party"]
            self.assertEquals(chair_party, 'Democratic Party')

            chair_state = response.json["news"][0]["chair_state"]
            self.assertEqual(chair_state, 'CA')

            chamber = response.json["news"][0]["chamber"]
            self.assertEqual(chamber, "House")

            congress = response.json["news"][0]["congress"]
            self.assertEqual(congress, '116')

            main_committee = response.json["news"][0]["main_committee"]
            self.assertEqual(main_committee, None)

            index = 1
            while("True"):
                assert response.json["news"][index]
                assert response.json["news"][index]["chair"]
                assert response.json["news"][index]["chair_party"]
                assert response.json["news"][index]["chair_state"]
                assert response.json["news"][index]["chamber"]
                assert response.json["news"][index]["congress"]
                assert response.json["news"][index]["id"]
                assert response.json["news"][index]["main_id"]

                index = index + 1
                if(index == 9):
                    break
            print("SUCCESS !")

if __name__ == "__main__":
    unittest.main()

