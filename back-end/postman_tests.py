import app
import unittest

class Tests(unittest.TestCase):
    def setUp(self):
        app.app.config["TESTING"] = True
        self.client = app.app.test_client()

    def testGetAllSenators(self):
        print("test to get all senators")
        with self.client:
            response = self.client.get("/api/get/senators")
            self.assertEqual(response.status_code, 200)

    def testGetAllCommittees(self):
        print("test to get all committees")
        with self.client:
            response = self.client.get("api/get/committees")
            self.assertEqual(response.status_code, 200)

    def testGetAllNews(self):
        print("test to get all news")
        with self.client:
            response = self.client.get("api/get/news")
            self.assertEqual(response.status_code, 200)

    def testGetSenatorInstance_1(self):
        print("test_1 to get senator by id")
        with self.client:
            response = self.client.get("api/get/senators/1")
            self.assertEqual(response.status_code, 200)

            name = response.json["news"]["name"]
            self.assertEqual(name, "Alma Adams")
    
    def testGetSenatorInstance_2(self):
        print("test_2 to get senator by id")
        with self.client:
            response = self.client.get("api/get/senators/23")
            self.assertEqual(response.status_code, 200)

            name = response.json["news"]["name"]
            self.assertEqual(name, "Donald Beyer")

    def testGetCommitteeInstance_1(self):
        print("test_1 to get committee by id")
        with self.client:
            response = self.client.get("api/get/committees/7")
            self.assertEqual(response.status_code, 200)

            chair = response.json["news"]["chair"]
            self.assertEqual(chair, "Collin C. Peterson")

            sub_id = response.json["news"]["sub_id"]
            self.assertEqual(sub_id, "HSAG14")

    def testGetCommitteeInstance_2(self):
        print("test_2 to get committee by id")
        with self.client:
            response = self.client.get("api/get/committees/12")
            self.assertEqual(response.status_code, 200)

            chair = response.json["news"]["chair"]
            self.assertEqual(chair, "Nita M. Lowey")

            sub_id = response.json["news"]["sub_id"]
            self.assertEqual(sub_id, "HSAP23")

    def testGetNewsInstance_1(self):
        print("test_1 to get news by id")
        with self.client:
            response = self.client.get("api/get/news/73")
            self.assertEqual(response.status_code, 200)

            committee_id = response.json["news"]["committee_id"]
            self.assertEqual(committee_id, "143 144 145 146 147 148 149 150 151 152 153 154 155 156 157 158 159 160")

            author = response.json["news"]["author"]
            self.assertEqual(author, "Matt Posky")

    def testGetNewsInstance_2(self):
        print("test_2 to get news by id")
        with self.client:
            response = self.client.get("api/get/news/24")
            self.assertEqual(response.status_code, 200)

            committee_id = response.json["news"]["committee_id"]
            self.assertEqual(committee_id, "172 143 144 145 146 147 148 149 150 151 152 153 154 155 156 193 194 195 196 197 198 199 200")

            author = response.json["news"]["author"]
            self.assertEqual(author, "Charles P. Pierce")
