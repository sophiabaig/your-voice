from services import app, db

def ins(results, total): 
    instances = [inst.serialize() for inst in results]
    user_ids = [user.id for user in results]
    return {
        "data": {
            "instances": instances,
            "total": total,
            "ids": user_ids
        }
    }

def rs(results): 
    return {
        "data": {
            "instance": results.serialize(),
        }
    }