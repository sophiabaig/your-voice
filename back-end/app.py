import json
import os
from services import app, db
from flask import request, jsonify
from sqlalchemy import update, or_, not_, and_, func
from sqlalchemy import create_engine, MetaData, Table, Column, Integer, String
from sqlalchemy.orm import sessionmaker
from endpoints.PopulatePoliticians import politician, populate_politicians, card as Politician, populate_rep_images
from endpoints.PopulateCommittees import committees, populate_main_committees, card as Committee
from endpoints.PopulateNews import news, populate_news, card as News
from functions import ins, rs
from sqlalchemy.sql.expression import desc, case

app.register_blueprint(politician)
app.register_blueprint(committees)
app.register_blueprint(news)

# App.route definitions
@app.route("/")
def hello():
    return "Welcome to the YourVoice backend!"

@app.route("/get/news")
def get_news():
    # get user arguments
    page = request.args.get('page', type=int)
    per_page = request.args.get('per_page', type=int)
    search_query = request.args.get('search', default='', type=str)
    query = News.query.filter(or_(News.title.ilike(f'%{search_query}%'), 
        News.description.ilike(f'%{search_query}%'), News.publisher.ilike(f'%{search_query}%'),
        News.author.ilike(f'%{search_query}%'), News.month.ilike(f'%{search_query}%')))
    publisher = request.args.get('publisher')
    month = request.args.get('month')
    title = request.args.get('title')
    date = request.args.get('date')
    author = request.args.get('author')

    # filtering and sorting
    if publisher is not None:
        query = query.filter(News.publisher.ilike(f'%{publisher}%'))
    if month is not None:
        query = query.filter(News.month.ilike(f'%{month}%'))
    if title is not None:
        if title == "Sort A-Z":
            query = query.order_by(News.title.asc())
        else:
            query = query.order_by(News.title.desc())            
    if author is not None:
        if author == "Sort A-Z":
            query = query.filter(author != None).order_by(News.author.asc())
        else:
            query = query.order_by(News.author.desc())
    if date is not None:
        if date == "Ascending Date":
            query = query.order_by(News.date.asc())
        else:
            query = query.order_by(News.date.desc())

    # pagination
    if page != None:
        pagination = query.order_by(News.not_filled).paginate(page=page, per_page=per_page)
        results = pagination.items
        total = pagination.total
    else:
        results = query.all()
        total = len(results)
        
    ret = ins(results, total)
    ret["news"] = ret["data"].pop("instances")
    ret["total"] = ret["data"].pop("total")
    return jsonify(ret)

@app.route("/get/committees")
def get_committees():
    # get user arguments
    page = request.args.get('page', type=int)
    per_page = request.args.get('per_page', type=int)
    search_query = request.args.get('search', default='', type=str)
    query = Committee.query.filter(or_(Committee.name.ilike(f'%{search_query}%'), Committee.chamber.ilike(f'%{search_query}%'),
        Committee.main_committee.ilike(f'%{search_query}%'), Committee.chair.ilike(f'%{search_query}%'),
        Committee.chair_party.ilike(f'%{search_query}%'), Committee.chair_state.ilike(f'%{search_query}%')))
    committee_type = request.args.get('committeeType')
    committee_chair = request.args.get('committeeChair')
    chair_party = request.args.get('chairParty')
    chair_state = request.args.get('chairState')
    committee_chamber = request.args.get('committeeChamber')

    # filtering and sorting
    if committee_type is not None:
        if committee_type == "Main Committee":
            query = query.filter_by(sub_id = None)
        else: 
            query = query.filter(not_(Committee.sub_id.is_(None)))
    if committee_chair is not None:
            query = query.filter(Committee.chair.ilike(f'%{committee_chair}%'))
    if chair_party is not None:
         query = query.filter(Committee.chair_party.ilike(f'%{chair_party}%'))
    states_dict = {"AL":"Alabama","AK":"Alaska","AZ":"Arizona","AR":"Arkansas","CA":"California","CO":"Colorado","CT":"Connecticut","DE":"Delaware","FL":"Florida","GA":"Georgia","HI":"Hawaii","ID":"Idaho","IL":"Illinois","IN":"Indiana","IA":"Iowa","KS":"Kansas","KY":"Kentucky","LA":"Louisiana","ME":"Maine","MD":"Maryland","MA":"Massachusetts","MI":"Michigan","MN":"Minnesota","MS":"Mississippi","MO":"Missouri","MT":"Montana","NE":"Nebraska","NV":"Nevada","NH":"New Hampshire","NJ":"New Jersey","NM":"New Mexico","NY":"New York","NC":"North Carolina","ND":"North Dakota","OH":"Ohio","OK":"Oklahoma","OR":"Oregon","PA":"Pennsylvania","RI":"Rhode Island","SC":"South Carolina","SD":"South Dakota","TN":"Tennessee","TX":"Texas","UT":"Utah","VT":"Vermont","VA":"Virginia","WA":"Washington","WV":"West Virginia","WI":"Wisconsin","WY":"Wyoming"}
    state_code = next((k for k, v in states_dict.items() if v == chair_state), None)
    if chair_state is not None:
         query = query.filter(Committee.chair_state.ilike(f'%{state_code}%'))
    if committee_chamber is not None:
         query = query.filter(Committee.chamber.ilike(f'%{committee_chamber}%'))
    
    # pagination
    if page != None:
        pagination = query.order_by(Committee.not_filled).paginate(page=page, per_page=per_page)
        results = pagination.items
        total = pagination.total
    else:
        results = query.all()
        total = len(results)
    ret = ins(results, total)
    ret["news"] = ret["data"].pop("instances")
    ret["total"] = ret["data"].pop("total")
    return jsonify(ret)

@app.route("/get/politicians")
def get_politicians():
    # get user arguments
    page = request.args.get('page', type=int)
    per_page = request.args.get('per_page', type=int)
    search_query = request.args.get('search', default='', type=str)
    query = Politician.query.filter(or_(Politician.name.ilike(f'%{search_query}%'), 
        Politician.position.ilike(f'%{search_query}%'), Politician.state.ilike(f'%{search_query}%'), 
        Politician.party.ilike(f'%{search_query}%'), Politician.district.ilike(f'%{search_query}%')))
    state = request.args.get('poliState')
    position = request.args.get('position')
    party = request.args.get('party')
    terms = request.args.get('numTerms')
    age = request.args.get('age')
    
    # sorting and filtering
    if state is not None:
        query = query.filter(Politician.state.ilike(f'%{state}%'))
    if position is not None:
        query = query.filter(Politician.position.ilike(f'%{position}%'))
    if party is not None:
        query = query.filter(Politician.party.ilike(f'%{party}%'))
    if terms is not None:
        terms_range = terms.split("-")
        query = query.filter(Politician.terms >= terms_range[0], Politician.terms <= terms_range[1])
    if age is not None:
        age_range = age.split("-")
        query = query.filter(Politician.age >= age_range[0], Politician.age <= age_range[1])
    
    # pagination
    if page != None:
        pagination = query.order_by(Politician.not_filled).paginate(page=page, per_page=per_page)
        results = pagination.items
        total = pagination.total
    else:
        results = query.all()
        total = len(results)
    ret = ins(results, total)
    ret["news"] = ret["data"].pop("instances")
    ret["total"] = ret["data"].pop("total")
    return jsonify(ret)

@app.route("/get/news/<id>")
def get_orgs_by_id(id):
    ret = News.query.get(id)
    ret = rs(ret)
    ret["news"]= ret["data"].pop("instance")
    return ret

@app.route("/get/politicians/<id>")
def get_politicians_by_id(id):
    ret = Politician.query.get(id)
    ret = rs(ret)
    ret["news"]= ret["data"].pop("instance")
    return ret

@app.route("/get/committees/<id>")
def get_committees_by_id(id):
    ret = Committee.query.get(id)
    ret = rs(ret)
    ret["news"]= ret["data"].pop("instance")
    return ret

def reset_db():
    print('reset db called')
    with app.app_context():
        db.session.remove()
        db.drop_all()
        db.create_all()

def create_db(table):
    with app.app_context():
        db.Model.metadata.tables[table].create(db.engine)

def delete_db(table):
    with app.app_context():
        db.Model.metadata.tables[table].drop(db.engine)