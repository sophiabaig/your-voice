echo "Deploying Backend..."
aws ecr get-login-password --region us-east-2 | docker login --username AWS --password-stdin 743340722972.dkr.ecr.us-east-2.amazonaws.com
docker build -t your-voice-backend .
docker tag your-voice-backend:latest 743340722972.dkr.ecr.us-east-2.amazonaws.com/your-voice-backend:latest
docker push 743340722972.dkr.ecr.us-east-2.amazonaws.com/your-voice-backend:latest
cd aws_deploy
eb deploy