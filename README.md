# YourVoice

## Team Members

| Name                 | EID     | GitLab ID      |
| -------------------- | ------- | -------------- |
| Sophia Baig          | stb996  | @sophiabaig    |
| Alejandro Arias Diaz | aa85837 | @jandroizer    |
| Cristian Astorga     | ca34467 | @CAstorga23    |
| Sheryl Dsouza        | sd38562 | @sheryl-dsouza |
| Dayou Ren            | dr37269 | @dayou919      |

## Git SHA

| Phase # | Git SHA                                  |
| ------- | ---------------------------------------- |
| 1       | 9a73afdcc0ce19b55df7bf50979fc0dedbf67d76 |
| 2       | 62a75ac949d4d031d9a22c9b4fe615435815be99 |
| 3       | e14068d9bc4057db64d84762477ac92669fef34f |
| 4       | 86055847cb80c22c10ab913396d53091fa176b6f |

## Project Leader

| Phase # | Phase Leader  |
| ------- | ------------- |
| 1       | Sophia Baig   |
| 2       | Sheryl Dsouza |
| 3       | Alejandro Arias Diaz |
| 4       | Cristian Astorga & Dayou Ren |

**Phase Leader Responsibilites:** Organize team meetings, final rubric walkthrough, project submission, assign tasks/issues to other team members.

## GitLab Pipelines

https://gitlab.com/sophiabaig/your-voice/-/pipelines

## Website Link

https://www.your-voice.me/

## Estimated and Actual Completion time

### Phase 1

| Name                 | Estimated Completion Time (hours) | Actual Completion Time (hours) |
| -------------------- | --------------------------------- | ------------------------------ |
| Sophia Baig          | 15                                | 25                             |
| Alejandro Arias Diaz | 10                                | 12                             |
| Cristian Astorga     | 8                                 | 10                             |
| Sheryl Dsouza        | 12                                | 22                             |
| Dayou Ren            | 7                                 | 10                             |

### Phase 2

| Name                 | Estimated Completion Time (hours) | Actual Completion Time (hours) |
| -------------------- | --------------------------------- | ------------------------------ |
| Sophia Baig          | 20                                | 32                             |
| Alejandro Arias Diaz | 18                                | 28                             |
| Cristian Astorga     | 18                                | 25                             |
| Sheryl Dsouza        | 18                                | 40                             |
| Dayou Ren            | 20                                | 15                             |

### Phase 3

| Name                 | Estimated Completion Time (hours) | Actual Completion Time (hours) |
| -------------------- | --------------------------------- | ------------------------------ |
| Sophia Baig          | 10                                | 15                             |
| Alejandro Arias Diaz | 12                                | 15                             |
| Cristian Astorga     | 15                                | 13                             |
| Sheryl Dsouza        | --                                | --                             |
| Dayou Ren            | --                                | --                             |

### Phase 

| Name                 | Estimated Completion Time (hours) | Actual Completion Time (hours) |
| -------------------- | --------------------------------- | ------------------------------ |
| Sophia Baig          | 10                                | 12                             |
| Alejandro Arias Diaz | --                                | --                             |
| Cristian Astorga     | 10                                | 8                              |
| Sheryl Dsouza        | --                                | --                             |
| Dayou Ren            | --                                | --                             |

## Comments

N/A

# Project Proposal

## Group Info

**IDB Group 22**

**Group Members:** Sophia Baig, Alejandro Arias Diaz, Cristian Astorga, Sheryl Dsouza, Dayou Ren

## Project Info

**Project Name:** YourVoice

**Project Proposal:** YourVoice is a website that allows users to explore the committees and subcommittees of the United States government, news articles related to the issues that the committees and subcommittees are responsible for, and the politicians that are members of such committees and subcommittess. This will help people who want to learn who in our government is responsible for shaping and passing laws and other types of legislation in the United States. Users will be able to view politicians, committees/subcommittees, and news instances, allowing for users to stay current and learn more about who and what to support within our government. The goal of YourVoice is to make political actions by our government more transparent and straight-forward.

### APIs

- ProPublica API (for Senate committees and subcommittees): https://projects.propublica.org/api-docs/congress-api/committees/
- Google Civic Information API (for politicians): https://developers.google.com/civic-information
- News API (for current news and legislation): https://newsdata.io/documentation
- Bing Image Search API (for retrieving photos of House Representatives): https://learn.microsoft.com/en-us/bing/search-apis/bing-image-search/overview

### Models

#### Committees and Subcommittees (within USA)

- **Estimated Instance Count:** ~250 subcommittees and committees
- **Attributes for fitering and sorting:** main committee name, committee chair, chair party, chair state, committee chamber
- **Media & more attributes:** videos, images, articles, citations
- **Connection to other models:** committees are made up of politicians and are responsible for certain issues or industries in the news

#### Politicians (within USA)

- **Estimated Instance Count:** ~550 politicians
- **Attributes for fitering and sorting:** state, party, position, birthplace, assumed office date
- **Media & more attributes:** images, embedded twitter feed, link to website, embedded facebook page
- **Connection to other models:** politicians are members of committees and subcommittees and are therefore responsible for creating legislation on certain issues in the news

#### News (within USA)

- **Estimated Instance Count:** ~11,000 articles
- **Attributes for fitering and sorting:** month of publishing, day of publishing, year of publishing, committee, publisher, author
- **Media & more attributes:** news articles, videos, podcasts, images, feeds
- **Connection to other models:** news articles are informative about certain industries or topics that a committee in congress is responsible for and therefore politicians that are a part of that committee are responsible for the issue as well

### Organizational Technique

For now, we are planning on using the same organizational technique that Professor Downing described in class - the one page per model with a grid of cards that have descriptions of the instances.

### Questions

1. Who is this politician and what committees are they a part of in Congress?
2. What has this committee or subcommittee done recently?
3. What is currently happening in the US related to the political system?
