import './App.css';
import GlobalNavbar from "./components/GlobalNavbar";
import { Route, Routes } from "react-router-dom";
import Home from "./pages/Home"
import About from "./pages/About"
import News from "./pages/News"
import NewsInstance from "./pages/NewsInstance"
import Senators from './pages/Senators';
import SenatorInstance from './pages/SenatorInstance';
import Committees from './pages/Committees';
import CommitteesInstance from './pages/CommitteesInstance';
import Visualizations from './pages/Visualizations';

function App() {
  return (
    <div>
      <GlobalNavbar />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/about" element={<About />} />
        <Route path="/politicians" element={<Senators />} />
        <Route path="/politicians/:id" element={<SenatorInstance />} />
        <Route path="/committees" element={<Committees />} />
        <Route path="/committees/:id" element={<CommitteesInstance />} />
        <Route path="/news" element={<News />} />
        <Route path="/news/:id" element={<NewsInstance />} />
        <Route path="visualizations" element={<Visualizations />} />
      </Routes>
    </div>
  );
}

export default App;
