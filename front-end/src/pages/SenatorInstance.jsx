import { Container, Row, Col, ListGroup, Button } from 'react-bootstrap';
import { useParams } from 'react-router';
import axios from "axios";
import { useState, useEffect } from 'react';
import { API_URL } from './URL';
import { Timeline } from 'react-twitter-widgets'
import { getCommittee, getNews } from "../api/api"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTwitter, faFacebook } from '@fortawesome/free-brands-svg-icons'
import placeholder from '../images/placeholders/politician.jpeg'

function SenatorsInstance() {
    const id = useParams().id ?? "1"
    const [senator, setSenatorId] = useState([]);
    const [committees, setCommittees] = useState([]);
    const [news_articles, setNews] = useState([]);
    const [overF, setOverF] = useState(false);
    const [overT, setOverT] = useState(false);
    const states_dict = { "AL": "Alabama", "AK": "Alaska", "AZ": "Arizona", "AR": "Arkansas", "CA": "California", "CO": "Colorado", "CT": "Connecticut", "DE": "Delaware", "FL": "Florida", "GA": "Georgia", "HI": "Hawaii", "ID": "Idaho", "IL": "Illinois", "IN": "Indiana", "IA": "Iowa", "KS": "Kansas", "KY": "Kentucky", "LA": "Louisiana", "ME": "Maine", "MD": "Maryland", "MA": "Massachusetts", "MI": "Michigan", "MN": "Minnesota", "MS": "Mississippi", "MO": "Missouri", "MT": "Montana", "NE": "Nebraska", "NV": "Nevada", "NH": "New Hampshire", "NJ": "New Jersey", "NM": "New Mexico", "NY": "New York", "NC": "North Carolina", "ND": "North Dakota", "OH": "Ohio", "OK": "Oklahoma", "OR": "Oregon", "PA": "Pennsylvania", "RI": "Rhode Island", "SC": "South Carolina", "SD": "South Dakota", "TN": "Tennessee", "TX": "Texas", "UT": "Utah", "VT": "Vermont", "VA": "Virginia", "WA": "Washington", "WV": "West Virginia", "WI": "Wisconsin", "WY": "Wyoming" };

    useEffect(() => {
        const fetchData = async () => {
            let response = await axios.get(
                `${API_URL}/get/politicians/${id}`
            );
            setSenatorId(response["data"]["news"])
        };
        fetchData();

        const fetchCommittees = async () => {
            let arr = senator.committee_id.split(" ");
            let committeesData = await Promise.all(
                arr.map((committee_id) => getCommittee(committee_id))
            );
            setCommittees(committeesData);
        };

        if (senator.committee_id) {
            fetchCommittees();
        }

        const fetchNews = async () => {
            let arr_news = senator.news_id.split(" ")
            let newsData = await Promise.all(
                arr_news.map((news_id) => getNews(news_id))
            );
            setNews(newsData);
        };
        if (senator.news_id) {
            fetchNews();
        }
    }, [id, senator.committee_id, senator.news_id]);

    const addPlaceholder = (event) => {
        event.currentTarget.src = placeholder;
    };

    return (
        <Container style={{ justifyContent: 'center' }} className="d-grid gap-3">
            <Row style={{ marginTop: '25px' }}>
                <Col>
                    <h1 style={{ align: 'center' }}>{senator.name}</h1>
                </Col>
            </Row>
            <Row className="mb-0">
                <Col>
                    <Container style={{ width: "640px", height: "auto", marginRight: "5px"}}>
                        <img alt="senator pic" variant="top" src={senator.image ? senator.image : placeholder} onError={addPlaceholder} style={{ width: "100%", height: "auto"}}></img>
                    </Container>
                </Col>
                <Col>
                    <Container style={{ width: "600px", height: "auto", marginLeft: "5px" }}>
                        <div style={{ display: "flex", alignItems: "center" }}>
                            {senator.position && <h3>{senator.position}<br></br></h3>}
                            {senator.position === "House Representative" && senator.link && (
                                <Button style={{ marginLeft: "50px", marginTop: "-10px" }} a href={senator.link} target="blank" variant="dark">
                                    Politician Website
                                </Button>
                            )}
                            {senator.position === "U.S. Senator" && senator.link && (
                                <Button style={{ marginLeft: "170px", marginTop: "-10px" }} a href={senator.link} target="blank" variant="dark">
                                    Politician Website
                                </Button>
                            )}
                            {senator.facebook && <span style={{ marginLeft: "15px", marginTop: "-10px" }} onMouseOver={() => setOverF(true)} onMouseLeave={() => setOverF(false)}>
                                <a href={"https://www.facebook.com/" + senator.facebook} target="blank">
                                    <FontAwesomeIcon icon={faFacebook} size="2xl" style={overF ? { color: "#293e6a" } : { color: "#3b5998" }} />
                                </a>
                            </span>}
                            {senator.twitter && <span style={{ marginLeft: "15px", marginTop: "-10px" }} onMouseOver={() => setOverT(true)} onMouseLeave={() => setOverT(false)}>
                                <a href={"https://twitter.com/" + senator.twitter} target="blank">
                                    <FontAwesomeIcon icon={faTwitter} size="2xl" style={overT ? { color: "#0078a7" } : { color: "#00acee" }} />
                                </a>
                            </span>}
                        </div>
                        {senator.state && <div><b>State: </b>{senator.state}{senator.district && <span>, District {senator.district}</span>} <br></br></div>}
                        {senator.party && <div><b>Party: </b>{senator.party} <br></br></div>}
                        {senator.age && <div><b>Age: </b>{senator.age}<br></br></div>}
                        {senator.terms && <div><b># of Years in Office: </b>{senator.terms}<br></br></div>}
                        {senator.phone && <div><b>Phone: </b>{senator.phone} <br></br></div>}
                        {senator.address && <div><b>Address: </b>{senator.address} <br></br></div>}
                        <br></br>
                        {senator.twitter && <Timeline
                            dataSource={{
                                sourceType: 'profile',
                                screenName: senator.twitter
                            }}
                            options={{ width: "600", height: "500" }}>
                        </Timeline>}
                    </Container>
                </Col>
            </Row>
            <br></br>
            <Row>
                <Col style={{ marginRight: '15px' }}>
                    <h4>Committee Assignments</h4>
                    {committees && committees.length > 0 ? (
                        <ListGroup variant="flush" style={{ maxHeight: "400px", overflowY: "auto", border: '1px solid darkgray', borderRadius: '5px' }}>
                            {committees.map((committee) => (
                                <ListGroup.Item action href={'/committees/' + committee.id} key={committee.id}>
                                    <h5>{committee.name}</h5>
                                    <h6>Committee Chamber: {committee.chamber}</h6>
                                    <h6>Chair: {committee.chair}</h6>
                                    <h6>Chair Party: {committee.chair_party}</h6>
                                    <h6>Chair State: {states_dict[committee.chair_state]}</h6>
                                </ListGroup.Item>
                            ))}
                        </ListGroup>
                    ) : (
                        <p>{senator.name} is not assigned any committees.</p>
                    )}
                </Col>
                <Col style={{ marginLeft: '15px' }}>
                    <h4>News Articles about {senator.name}</h4>
                    {news_articles && news_articles.length > 0 ? (
                        <ListGroup variant="flush" style={{ maxHeight: "400px", overflowY: "auto", border: '1px solid darkgray', borderRadius: '5px' }}>
                            {news_articles.map((article) => (
                                <ListGroup.Item action href={'/news/' + article.id} key={article.id}>
                                    <h5>{article.title}</h5>
                                    <h6>Author: {article.author}</h6>
                                    <h6>Publisher: {article.publisher}</h6>
                                </ListGroup.Item>
                            ))}
                        </ListGroup>
                    ) : (
                        <p>No news articles found about {senator.name}.</p>
                    )}
                </Col>
            </Row>
            <br></br>
        </Container>
    )
}

export default SenatorsInstance;
