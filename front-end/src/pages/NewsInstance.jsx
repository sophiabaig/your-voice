import { Container, Row, Col, ListGroup, Button } from 'react-bootstrap';
import { useParams } from 'react-router';
import axios from "axios";
import { useState, useEffect } from 'react';
import { API_URL } from './URL';
import { Timeline } from 'react-twitter-widgets'
import { getCommittee, getPolitician } from "../api/api"
import { news_publishers_twitter_list } from "../data/NewsPublisherTwitterHandles"
import placeholder from '../images/placeholders/news.jpeg';

function NewsInstance() {
    const id = useParams().id ?? "1"
    const [news, setNewsId] = useState([]);
    const [senators, setSenators] = useState([]);
    const [committees, setCommittees] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            let response = await axios.get(
                `${API_URL}/get/news/${id}`
            );
            setNewsId(response["data"]["news"])
        };
        fetchData();

        const fetchCommittees = async () => {
            let arr = news.committee_id.split(" ");
            let committeesData = await Promise.all(
                arr.map((committee_id) => getCommittee(committee_id))
            );
            setCommittees(committeesData);
        };
        if (news.committee_id) {
            fetchCommittees();
        }

        const fetchSenators = async () => {
            let arr_senators = news.senator_id.split(" ");
            let senatorData = await Promise.all(
                arr_senators.map((senator_id) => getPolitician(senator_id))
            );
            setSenators(senatorData);
        };
        if (news.senator_id) {
            fetchSenators();
        }
    }, [id, news.committee_id, news.senator_id]);

    function removeHTMLTags(htmlString) {
        const doc = new DOMParser().parseFromString(htmlString, 'text/html');
        return doc.body.textContent || '';
    }

    const addPlaceholder = (event) => {
        event.currentTarget.src = placeholder;
    };

    return (
        <Container style={{ justifyContent: 'center' }} className="d-grid gap-3">
            <Row style={{ marginTop: '25px' }}>
                <Col>
                    <h1 style={{ align: 'center' }}> {news.title} </h1>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Container style={{ width: "600px", height: "auto", marginRight: "5px" }}>
                        <img alt="news pic" variant="top" src={news.image ? news.image : placeholder} onError={addPlaceholder} style={{ width: "100%", height: "auto" }}></img>
                    </Container>
                </Col>
                <Col>
                    <Container style={{ width: "600px", height: "auto", marginLeft: "5px" }}>
                        <div className="mb-3">
                            {news.link && <Button a href={news.link} variant="dark" target="blank">Article Link</Button>}
                        </div>
                        {news.publisher && <div><b>Publisher: </b>{news.publisher} <br></br></div>}
                        {news.author && <div><b>Author: </b>{news.author} <br></br></div>}
                        {news.month && <div><b>Month Published: </b>{news.month} <br></br></div>}
                        {news.year && <div><b>Year: </b>{news.year} <br></br></div>}
                        {news.date && <div><b>Date: </b>{new Date(news.date).toLocaleDateString("en-US", { month: "long", day: "numeric", year: "numeric" })} <br></br></div>}
                        <div><b>Content: </b>{removeHTMLTags(news.content)} <br></br></div>
                        <br></br>
                    </Container>
                </Col>
            </Row>
            <br></br>
            {news.publisher && news_publishers_twitter_list.hasOwnProperty(news.publisher) ? (
                <Row className="mt-0 mx-0">
                    <Col style={{ marginRight: '15px' }}>
                        <Timeline
                            dataSource={{
                                sourceType: 'profile',
                                screenName: news_publishers_twitter_list[news.publisher]
                            }}
                            options={{ width: "600", height: "500" }}
                        />
                    </Col>
                    <Col>
                        {senators[0] && senators[0].twitter ? (
                            <Timeline
                                dataSource={{
                                    sourceType: 'profile',
                                    screenName: senators[0].twitter,
                                }}
                                options={{ width: '600', height: '500' }}
                            ></Timeline>
                        ) : committees[0] && committees[0].twitter ? (
                            <Timeline
                                dataSource={{
                                    sourceType: 'profile',
                                    screenName: committees[0].twitter,
                                }}
                                options={{ width: '600', height: '500' }}
                            ></Timeline>
                        ) : null}
                    </Col>
                </Row>
            ) : (
                <Row className="mt-0 mx-0">
                    <Col>
                        {senators[0] && senators[0].twitter && <Timeline
                            dataSource={{
                                sourceType: 'profile',
                                screenName: senators[0].twitter
                            }}
                            options={{ width: "600", height: "500" }}>
                        </Timeline>}
                    </Col>
                    <Col>
                        {committees[0] && committees[0].twitter && <Timeline
                            dataSource={{
                                sourceType: 'profile',
                                screenName: committees[0].twitter
                            }}
                            options={{ width: "600", height: "500" }}>
                        </Timeline>}
                    </Col>
                </Row>
            )}
            <br></br>
            <Row>
                <Col style={{ marginRight: '15px' }}>
                    <h4>Relevant Politicians</h4>
                    {senators && senators.length > 0 ? (
                        <ListGroup variant="flush" style={{ maxHeight: "400px", overflowY: "auto", border: '1px solid darkgray', borderRadius: '5px' }}>
                            {senators.map((senator) => (
                                <ListGroup.Item action href={'/politicians/' + senator.id} key={senator.id}>
                                    <h5>{senator.name}</h5>
                                    <h6>{senator.position}</h6>
                                    <h6>Party: {senator.party}</h6>
                                </ListGroup.Item>
                            ))}
                        </ListGroup>
                    ) : (
                        <div>This news article has no relevant politicians.</div>
                    )}
                </Col>
                <Col style={{ marginLeft: "15px" }}>
                    <h4>Relevant Committees</h4>
                    {committees && committees.length > 0 ? (
                        <ListGroup variant="flush" style={{ maxHeight: "400px", overflowY: "auto", border: '1px solid darkgray', borderRadius: '5px' }}>
                            {committees.map((committee) => (
                                <ListGroup.Item action href={'/committees/' + committee.id} key={committee.id}>
                                    <h5>{committee.name}</h5>
                                    <h6>Committee Chamber: {committee.chamber}</h6>
                                    <h6>Chair: {committee.chair}</h6>
                                    <h6>Chair Party: {committee.chair_party}</h6>
                                    <h6>Chair State: {committee.chair_state}</h6>
                                </ListGroup.Item>
                            ))}
                        </ListGroup>
                    ) : (
                        <div>This news article has no relevant committees.</div>
                    )}
                </Col>
            </Row>
            <br></br>
        </Container>
    )
}

export default NewsInstance;
