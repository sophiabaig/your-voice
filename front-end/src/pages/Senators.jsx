import { useState, useEffect } from 'react';
import SenatorCard from "../components/Cards/SenatorCard"
import axios from "axios";
import { API_URL } from './URL';
import { Container, Form, FloatingLabel, Col, Row, Button, Pagination } from 'react-bootstrap'
import Filter from "../components/Filter"
import RangeSlider from "../components/RangeSlider"

function Senators() {
  const states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming']
  const perPage = 9;
  const [senators, setSenators] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);
  const [totalInstances, setTotalInstances] = useState(0);
  const [searchQuery, setSearchQuery] = useState("");

  // Pagination
  function handlePage(num) {
    setCurrentPage(num)
  }

  let items = []
  for (let num = currentPage - 1; num <= currentPage + 1; num++) {
    if (num > 0 && num <= totalPages) {
      items.push(
        <Pagination.Item
          key={num}
          onClick={() => handlePage(num)}
          active={num === currentPage}>
          {num}
        </Pagination.Item>
      )
    }
  }

  // Slider hooks
  const [numTerms, setNumTerms] = useState([0, 50]);
  const handleNumTermsFilter = (value) => {
    setCurrentPage(1);
    setNumTerms(value);
  }
  const [age, setAge] = useState([0, 100]);
  const handleAgeFilter = (value) => {
    setCurrentPage(1);
    setAge(value);
  }

  // Filter hooks
  const [poliState, setPoliState] = useState("");
  const handlePoliStateFilter = (value) => {
    setCurrentPage(1);
    setPoliState(value);
  }
  const [position, setPosition] = useState("");
  const handlePositionFilter = (value) => {
    setCurrentPage(1);
    setPosition(value);
  }
  const [party, setParty] = useState("");
  const handlePartyFilter = (value) => {
    setCurrentPage(1);
    setParty(value);
  }

  // check if range slider arrays are equal
  function arrayEquals(a, b) {
    return (
      Array.isArray(a) &&
      Array.isArray(b) &&
      a.length === b.length &&
      a.every((val, index) => val === b[index])
    );
  }

  useEffect(() => {
    const fetchData = async () => {
      var queryURL = `${API_URL}/get/politicians?page=${currentPage}&per_page=${perPage}`
      if (searchQuery !== "") {
        queryURL += `&search=${searchQuery}`
      }
      if (poliState !== "") {
        queryURL += `&poliState=${poliState}`
      }
      if (position !== "") {
        queryURL += `&position=${position}`
      }
      if (party !== "") {
        queryURL += `&party=${party}`
      }
      if (!arrayEquals(numTerms, [0, 50])) {
        queryURL += `&numTerms=${numTerms[0]}-${numTerms[1]}`
      }
      if (!arrayEquals(age, [0, 100])) {
        queryURL += `&age=${age[0]}-${age[1]}`
      }
      let response = await axios.get(queryURL);
      setSenators(response["data"]["news"])
      setTotalPages(Math.ceil(response["data"]["total"] / perPage))
      setTotalInstances(response["data"]["total"])
    };
    fetchData();
  }, [currentPage, perPage, searchQuery, poliState, position, party, numTerms, age]);

  const handleSearch = (event) => {
    setSearchQuery(event.target.value);
  }
  const handleSearchSubmit = (event) => {
    event.preventDefault();
  };
  function handleReset() {
    window.location.reload();
  }

  return (
    <Container className="mt-5 mb-4">
      {/* Input search Bar */}
      <Form onSubmit={handleSearchSubmit}>
        <FloatingLabel
          controlId="searchInput"
          label="Search for a politican!"
          className="mb-3">
          <Form.Control type="search" onChange={handleSearch} value={searchQuery} />
        </FloatingLabel>
      </Form>
      {/* Filtering */}
      <Form className="d-flex gap-4 justify-content-center pb-2 mt-5">
        <span style={{ marginTop: "-15px" }}><Form.Label># of Years in Office</Form.Label></span>
        <RangeSlider min={0} max={50} onChange={handleNumTermsFilter} discrete></RangeSlider>
        <br></br>
        <Form.Label>Age</Form.Label>
        <RangeSlider min={0} max={100} onChange={handleAgeFilter} discrete></RangeSlider>
      </Form>
      <Form className="d-flex gap-4 justify-content-center pb-2">
        <Filter title="State" items={states} scroll onChange={handlePoliStateFilter}></Filter>
        <Filter title="Position" items={["Senator", "House Representative"]} onChange={handlePositionFilter}></Filter>
        <Filter title="Party" items={["Democratic Party", "Republican Party"]} onChange={handlePartyFilter}></Filter>
        <Button variant="primary" onClick={handleReset}>Reset</Button>
      </Form>
      <Row className="mb-3 justify-content-center">
        {senators != null && senators.map((data) => {
          return (
            <Col>
              <SenatorCard
                id={data.id}
                name={data.name}
                position={data.position}
                state={data.state}
                party={data.party}
                image={data.image}
                facebook={data.facebook}
                twitter={data.twitter}
                terms={data.terms}
                age={data.age}
                searchQuery={searchQuery}
                district={data.district}>
              </SenatorCard>
            </Col>
          )
        })}
      </Row>
      <Pagination className="justify-content-center mt-5">
        {currentPage > 2 && (
          <Pagination.First
            key={1}
            onClick={() => handlePage(1)}
            active={1 === currentPage}>
            1
          </Pagination.First>
        )}
        {currentPage > 3 && <Pagination.Ellipsis />}
        {items}
        {currentPage < totalPages - 2 && <Pagination.Ellipsis />}
        {currentPage < totalPages - 1 && (
          <Pagination.Last
            key={totalPages}
            onClick={() => handlePage(totalPages)}
            active={totalPages === currentPage}>
            {totalPages}
          </Pagination.Last>
        )}
      </Pagination>
      <div style={{ textAlign: "center" }}>Showing {currentPage === totalPages ? 
        (totalInstances === 0 ? 0 : (totalInstances % perPage === 0 ? perPage : totalInstances % perPage)) : 
        (totalInstances === 0 ? 0 : perPage)} of {totalInstances} politicians</div>
    </Container>
  )
};

export default Senators;