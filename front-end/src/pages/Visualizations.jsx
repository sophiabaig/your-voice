import React from 'react';
import { Container, Stack } from 'react-bootstrap'
import NewsPerPublisher from "../data/d3_visualization/NewsPerPublisher"
import PoliticianAgeDist from '../data/d3_visualization/PoliticianAgeDist';
import CommitteesParty from '../data/d3_visualization/CommitteesParty';
import ProviderNumPostsPerCategory from '../data/d3_visualization/ProviderNumPostsPerCategory';
import ProviderFrequencyOfTags from '../data/d3_visualization/ProviderFrequencyOfTags';
import ProviderNumBillsPerState from '../data/d3_visualization/ProviderNumBillsPerState';

function Visualizations() {
    return (
        <Stack>
            <Container
                style={{
                    backgroundColor: "#abdfed",
                    textAlign: "center",
                    paddingTop: "10px",
                    maxWidth: "100%",
                    height: "15vh"
                }}>
                <h1 style={{ fontSize: "45px" }} className="d-flex justify-content-center p-4 ">Visualizations</h1>
            </Container>
            <Container
                style={{ height: "78vh" }}>
                <NewsPerPublisher></NewsPerPublisher>
            </Container>
            <Container className="bottom"
                style={{
                    backgroundColor: "#abdfed",
                    textAlign: "center",
                    paddingTop: "60px",
                    maxWidth: "100%",
                    height: "90vh"
                }}>
                <PoliticianAgeDist></PoliticianAgeDist>
            </Container>
            <Container className='bot'
                style={{ 
                    textAlign: "center",
                    height: "78vh",
                     }}>
                <CommitteesParty></CommitteesParty>
            </Container>
            <Container
                style={{
                    backgroundColor: "#abdfed",
                    textAlign: "center",
                    paddingTop: "10px",
                    maxWidth: "100%",
                    height: "15vh"
                }}>
                <h1 className="d-flex justify-content-center p-4 ">Provider Visualizations</h1>
            </Container>
            <Container
                style={{ height: "78vh" }}>
                <ProviderNumPostsPerCategory></ProviderNumPostsPerCategory>
            </Container>
            <Container className="bottom"
                style={{
                    backgroundColor: "#abdfed",
                    textAlign: "center",
                    paddingTop: "5px",
                    maxWidth: "100%",
                    height: "80vh"
                }}>
                <ProviderFrequencyOfTags></ProviderFrequencyOfTags>
            </Container>
            <Container
                style={{ height: "78vh" }}>
                <ProviderNumBillsPerState></ProviderNumBillsPerState>
            </Container>
        </Stack>
    )
}

export default Visualizations;