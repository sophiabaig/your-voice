import { Container, Row, Col, ListGroup } from 'react-bootstrap';
import { useParams } from 'react-router';
import axios from "axios";
import { useState, useEffect } from 'react';
import { API_URL } from './URL';
import { getPolitician, getNews } from "../api/api"
import { Timeline } from 'react-twitter-widgets'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTwitter } from '@fortawesome/free-brands-svg-icons'
import placeholder from '../images/placeholders/committees.png';

function CommitteeInstance() {
    const id = useParams().id ?? "1"
    const [committee, setCommittee] = useState([]);
    const [senators, setSenators] = useState([]);
    const [news_articles, setNews] = useState([]);
    const [overT, setOverT] = useState(false);
    const states_dict = { "AL": "Alabama", "AK": "Alaska", "AZ": "Arizona", "AR": "Arkansas", "CA": "California", "CO": "Colorado", "CT": "Connecticut", "DE": "Delaware", "FL": "Florida", "GA": "Georgia", "HI": "Hawaii", "ID": "Idaho", "IL": "Illinois", "IN": "Indiana", "IA": "Iowa", "KS": "Kansas", "KY": "Kentucky", "LA": "Louisiana", "ME": "Maine", "MD": "Maryland", "MA": "Massachusetts", "MI": "Michigan", "MN": "Minnesota", "MS": "Mississippi", "MO": "Missouri", "MT": "Montana", "NE": "Nebraska", "NV": "Nevada", "NH": "New Hampshire", "NJ": "New Jersey", "NM": "New Mexico", "NY": "New York", "NC": "North Carolina", "ND": "North Dakota", "OH": "Ohio", "OK": "Oklahoma", "OR": "Oregon", "PA": "Pennsylvania", "RI": "Rhode Island", "SC": "South Carolina", "SD": "South Dakota", "TN": "Tennessee", "TX": "Texas", "UT": "Utah", "VT": "Vermont", "VA": "Virginia", "WA": "Washington", "WV": "West Virginia", "WI": "Wisconsin", "WY": "Wyoming" }

    useEffect(() => {
        const fetchData = async () => {
            let response = await axios.get(
                `${API_URL}/get/committees/${id}`
            );
            setCommittee(response["data"]["news"])
        };
        fetchData();

        const fetchSenators = async () => {
            let arr_senators = committee.senator_id.split(" ");
            let senatorData = await Promise.all(
                arr_senators.map((senator_id) => getPolitician(senator_id))
            );
            setSenators(senatorData);
        };

        if (committee.senator_id) {
            fetchSenators();
        }

        const fetchNews = async () => {
            let arr_news = committee.news_id.split(" ")
            if (arr_news.length > 15) {
                arr_news = committee.news_id.split(" ").slice(0, 15)
            }
            let newsData = await Promise.all(
                arr_news.map((news_id) => getNews(news_id))
            );
            setNews(newsData);
        };

        if (committee.news_id) {
            fetchNews();
        }

    }, [id, committee.senator_id, committee.news_id]);

    const addPlaceholder = (event) => {
        event.currentTarget.src = placeholder;
    };

    return (
        <Container style={{ justifyContent: 'center' }} className="d-grid gap-3">
            <Row style={{ marginTop: '25px' }}>
                <Col>
                    {committee.main_committee ? (
                        <h1 style={{ align: "center" }}> Subcommittee on {committee.name} </h1>
                    ) : (
                        <h1 style={{ align: "center" }}>{committee.name}</h1>
                    )}
                </Col>
            </Row>
            <Row className="mb-0">
                <Col>
                    <Container style={{ width: "600px", height: "auto" }}>
                        <img alt="committee pic" variant="top" src={committee.image ? committee.image : placeholder} onError={addPlaceholder} style={{ width: "100%", height: "auto", backgroundColor: '#D3D3D3', borderRadius: '0.25rem' }} className="p-4"></img>
                    </Container>
                </Col>
                <Col>
                    <Container style={{ width: "600px", height: "auto", marginLeft: "5px" }}>
                        <div className="mb-2">
                            {committee.twitter && <span onMouseOver={() => setOverT(true)} onMouseLeave={() => setOverT(false)}>
                                <a href={"https://twitter.com/" + committee.twitter} target="blank">
                                    <FontAwesomeIcon icon={faTwitter} size="2xl" style={overT ? { color: "#0078a7" } : { color: "#00acee" }} />
                                </a>
                            </span>}
                        </div>
                        {committee.chamber && <div><b>Chamber: </b>{committee.chamber} <br></br></div>}
                        {committee.main_committee ? (
                            <div><b>Committee Type: </b>Subcommittee</div>
                        ) : (
                            <div><b>Committee Type: </b>Main Committee</div>
                        )}
                        {committee.main_committee ? (
                            <div><b>Main Committee: </b>{committee.main_committee}</div>
                        ) : (
                            <div><b>Subcommittees: </b></div>
                        )}
                        {committee.chair && <div><b>Chair: </b>{committee.chair} <br></br></div>}
                        {committee.chair_party && <div><b>Chair Party: </b>{committee.chair_party} <br></br></div>}
                        {committee.chair_state && <div><b>Chair State: </b>{states_dict[committee.chair_state]} <br></br></div>}
                    </Container>
                </Col>
            </Row>
            <br></br>
            <Row>
                <Col>
                    <h4>Relevant Politicians</h4>
                    {senators && senators.length > 0 ? (
                        <ListGroup variant="flush" style={{ maxHeight: "600px", overflowY: "auto", border: '1px solid darkgray', borderRadius: '5px' }}>
                            {senators.map((senator) => (
                                <ListGroup.Item action href={'/politicians/' + senator.id} key={senator.id}>
                                    <h5>{senator.name}</h5>
                                    <h6>{senator.position}</h6>
                                    <h6>Party: {senator.party}</h6>
                                </ListGroup.Item>
                            ))}
                        </ListGroup>
                    ) : (
                        <p>There are no relevant politicians for this committee.</p>
                    )}
                </Col>
                <Col>
                    <h4>Relevant News</h4>
                    {news_articles && news_articles.length > 0 ? (
                        <ListGroup variant="flush" style={{ maxHeight: "600px", overflowY: "auto", border: '1px solid darkgray', borderRadius: '5px' }}>
                            {news_articles.map((article) => (
                                <ListGroup.Item action href={'/news/' + article.id} key={article.id}>
                                    <h5>{article.title}</h5>
                                    <h6>Author: {article.author}</h6>
                                    <h6>Publisher: {article.publisher}</h6>
                                </ListGroup.Item>
                            ))}
                        </ListGroup>
                    ) : (
                        <p>There are no relevant news articles for this committee.</p>
                    )}
                </Col>
                <Col style={{marginTop: "10px"}}>
                    <br></br>
                    {committee.twitter && <Timeline
                        dataSource={{
                            sourceType: 'profile',
                            screenName: committee.twitter
                        }}
                        options={{ width: "450", height: "600" }}>
                    </Timeline>}
                </Col>
            </Row>
            <br></br>
        </Container>
    )
}

export default CommitteeInstance;
