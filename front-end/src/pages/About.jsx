// This file is rougly inspired by the GeoJobs Project. https://gitlab.com/sarthaksirotiya/cs373-idb/-/blob/main/front-end/src/views/About.jsx

import { useState, useEffect } from "react";
import { Row, Col, Container } from 'react-bootstrap'
import { Link } from "react-router-dom"
import axios from "axios";
import TeamCard from "../components/Cards/TeamCard";
import ToolCard from "../components/Cards/ToolCard"
import APICard from "../components/Cards/APICard"
import { teamData } from "../data/TeamData";
import { tools_list } from "../data/ToolData";
import { api_list } from "../data/APIData";

const client = axios.create({
  baseURL: "https://gitlab.com/api/v4/",
});

const fetchGitLabData = async () => {
  let totalCommits = 0,
    totalIssues = 0,
    totalUnitTests = 0;

  teamData.forEach((member) => {
    member.commits = 0;
    member.issues = 0;
    totalUnitTests += member.unit_tests;
  });

  await client
    .get("projects/43390395/repository/contributors?per_page=500")
    .then((response) => {
      response.data.forEach((element) => {
        const { name, email, commits } = element;
        teamData.forEach((member) => {
          if (member.name === name || member.name2 === name || member.email === email || member.gitlab_username === name) {
            member.commits += commits;
          }
        });
        totalCommits += commits;
      });
    });

  await client.get("projects/43390395/issues?per_page=200").then((response) => {
    response.data.forEach((element) => {
      const { assignees } = element;
      assignees.forEach((assignee) => {
        const { name, username } = assignee;
        teamData.forEach((member) => {
          if (
            member.name === name ||
            member.name2 === name ||
            member.gitlab_username === username
          )
            member.issues += 1;
        });
      });
      totalIssues += 1;
    });
  });

  return {
    totalCommits: totalCommits,
    totalIssues: totalIssues,
    totalTests: totalUnitTests,
    teamData: teamData,
  };
};

function About() {
  const [teamList, setTeamList] = useState([]);
  const [totalCommits, setTotalCommits] = useState(0);
  const [totalIssues, setTotalIssues] = useState(0);
  const [totalTests, setTotalTests] = useState(0);

  useEffect(() => {
    const fetchData = async () => {
      if (teamList === undefined || teamList.length === 0) {
        const gitlabInfo = await fetchGitLabData();
        setTotalCommits(gitlabInfo.totalCommits);
        setTotalIssues(gitlabInfo.totalIssues);
        setTotalTests(gitlabInfo.totalTests);
        setTeamList(gitlabInfo.teamInfo);
      }
    };
    fetchData();
  }, [teamList]);

  return (
    <Container className="mb-5">
      <Row className="text-center">
        <h1 style={{ fontSize: "45px" }} className="d-flex justify-content-center mt-4">About YourVoice</h1>
        <p className="px-5">YourVoice is a website that provides users with information on
          politicians, committees, and news regarding the political landscape of the United
          States government. YourVoice aims to make political actions by our government more
          transparent and straight-forward.</p>
      </Row>
      <br></br>
      <Row className="text-center" style={{ backgroundColor: "#abdfed" }}>
        <h1 style={{ fontSize: "45px" }} className="d-flex justify-content-center mt-4">How YourVoice Works</h1>
        <p className="pb-3">YourVoice integrates data from the Google Civic Information API, ProPublica API, and News API to provide users
          with information about who is involved in our government, what they are involved in, and what is currently happening within US politics.
          Users will be able to view information about politicians, committees, and political news, which will allow users to stay current and
          learn about who/what to support within our government.
        </p>
      </Row>
      <br></br>
      <h1 style={{ fontSize: "45px" }} className="d-flex justify-content-center mt-2 mb-1">Meet the Team</h1>
      <Row xs={1} sm={2} md={3} xl={3} className="mb-3 justify-content-center">
        {teamData.map((data) => {
          return (
            <Col>
              <TeamCard
                name={data.name}
                gitlab_username={data.gitlab_username}
                image={data.image}
                role={data.role}
                bio={data.bio}
                commits={data.commits}
                issues={data.issues}
                unit_tests={data.unit_tests}>
              </TeamCard>
            </Col>
          )
        })}
      </Row>
      <br></br>
      <Row className="text-center pb-3" style={{ backgroundColor: "#abdfed" }}>
        <Row>
          <h1 style={{ fontSize: "45px" }} className="d-flex justify-content-center m-4">Repository Statistics</h1>
          <Col>
            <h2>Total Commits: {totalCommits}</h2>
          </Col>
          <Col>
            <h2>Total Issues: {totalIssues}</h2>
          </Col>
          <Col>
            <h2>Total Unit Tests: {totalTests}</h2>
          </Col>

        </Row>
        <Row>
          <Col style={{marginRight:"-200px"}}>
            <h2 className="mt-3"><Link to="https://gitlab.com/sophiabaig/your-voice" target="_blank">GitLab Repository</Link></h2>
          </Col>
          <Col>
            <h2 className="mt-3"><Link to="https://documenter.getpostman.com/view/25880146/2s93CExwyb" target="_blank">API Documentation</Link></h2>
          </Col>
        </Row>
      </Row>
      <br></br>
      <h1 style={{ fontSize: "45px" }} className='text-center my-3'>Tools</h1>
      <Row>
        {tools_list.map((data) => (
          <Col xs={12} sm={6} md={4}>
            <ToolCard
              title={data.title}
              image={data.image}
              description={data.description}
              link={data.link}
            ></ToolCard>
          </Col>
        ))}
      </Row>
      <br></br>
      <br></br>
      <h1 style={{ fontSize: "45px" }} className="text-center my-3">APIs</h1>
      <Row>
        {api_list.map((data) => (
          <Col xs={12} sm={6} md={4}>
            <APICard
              title={data.title}
              image={data.image}
              description={data.description}
              link={data.link}>
            </APICard>
          </Col>
        ))}
      </Row>
    </Container>
  );
}

export default About;