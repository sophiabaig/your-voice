import { Container, Row, Col, Carousel } from 'react-bootstrap'
import HomeNavCard from "../components/Cards/HomeNavCard";

function Home() {
    return (
        <div>
            <div style={{ position: "relative", height: "70vh" }}>
                <Carousel>
                    <Carousel.Item>
                        <img className="d-block w-100"
                            alt="protest"
                            style={{ width: "100%", height: "70vh", objectFit: "cover", objectPosition: "center top" }}
                            src="https://media-cldnry.s-nbcnews.com/image/upload/newscms/2020_23/3378141/200603-washington-protest-peaceful-ac-1002p.jpg">
                        </img>
                    </Carousel.Item>
                    <Carousel.Item>
                        <img className="d-block w-100"
                            alt="representatives"
                            style={{ width: "100%", height: "70vh", objectFit: "cover", objectPosition: "center 60%" }}
                            src="https://media.npr.org/assets/img/2021/04/19/gettyimages-1231493896-a83e62d6e8ad8edea13451f44df9c01843e4268f-s1600-c85.webp">
                        </img>
                    </Carousel.Item>
                    <Carousel.Item>
                        <img className="d-block w-100"
                            alt="voting booth line"
                            style={{ width: "100%", height: "70vh", objectFit: "cover", objectPosition: "center center" }}
                            src="https://www.journalofdemocracy.org/wp-content/uploads/2021/04/32.2-Persily-photo-1-scaled-e1640725046820.jpg">
                        </img>
                    </Carousel.Item>
                </Carousel>
                <div style={{ position: "absolute", top: 0, left: 0, right: 0, bottom: 0, display: "flex", alignItems: "center", justifyContent: "center", flexDirection: "column" }}>
                    <h1 style={{ textShadow: '0 0 35px black, 0 0 35px black', fontSize: 60, fontWeight: 500, color: "#ffffff", textAlign: "center", marginBottom: 20 }}>Welcome to YourVoice</h1>
                    <h2 style={{ textShadow: '0 0 35px black, 0 0 35px black', color: "#ffffff", textAlign: "center" }}>Empowering you with political knowledge for a more transparent government.</h2>
                </div>
            </div>
            <Container className="my-4">
                <Row>
                    <h2 className="text-center mb-3">What's next?</h2>
                    <Col>
                        <HomeNavCard
                            title="Politicians"
                            description="Who are the people behind the government and what are they responsible for?"
                            img="https://imageio.forbes.com/blogs-images/kriskrane/files/2018/11/worst-senators-01-1200x800.jpg?format=jpg&width=960"
                            link="/politicians">
                        </HomeNavCard>
                    </Col>
                    <Col>
                        <HomeNavCard
                            title="Committees"
                            description="What kind of committees exist in the government and who oversees them?"
                            img="https://static01.nyt.com/images/2022/06/14/us/politics/14dc-jan6-copy/merlin_208512735_55e0b772-ef36-44e8-87df-fbb5b5983815-superJumbo.jpg"
                            link="/committees">
                        </HomeNavCard>
                    </Col>
                    <Col>
                        <HomeNavCard
                            title="News"
                            description="What does the political landscape of the United States look like currently?"
                            img="https://static01.nyt.com/images/2022/08/10/climate/10CLI-PROTESTERS-newtop/merlin_210488148_89a632e9-bf50-4954-ab32-4f31632dba7f-articleLarge.jpg?quality=75&auto=webp&disable=upscale"
                            link="/news">
                        </HomeNavCard>
                    </Col>
                </Row>
            </Container>
        </div>
    );
};

export default Home;