import { useState, useEffect } from 'react';
import CommitteesCard from "../components/Cards/CommitteesCard"
import axios from "axios";
import { API_URL } from './URL';
import { Container, Form, FloatingLabel, Col, Row, Button, Pagination } from 'react-bootstrap'
import Filter from "../components/Filter"
import { chair_list } from "../data/CommitteeChairs";

function Committees() {
  const states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'];
  const states_dict = { "AL": "Alabama", "AK": "Alaska", "AZ": "Arizona", "AR": "Arkansas", "CA": "California", "CO": "Colorado", "CT": "Connecticut", "DE": "Delaware", "FL": "Florida", "GA": "Georgia", "HI": "Hawaii", "ID": "Idaho", "IL": "Illinois", "IN": "Indiana", "IA": "Iowa", "KS": "Kansas", "KY": "Kentucky", "LA": "Louisiana", "ME": "Maine", "MD": "Maryland", "MA": "Massachusetts", "MI": "Michigan", "MN": "Minnesota", "MS": "Mississippi", "MO": "Missouri", "MT": "Montana", "NE": "Nebraska", "NV": "Nevada", "NH": "New Hampshire", "NJ": "New Jersey", "NM": "New Mexico", "NY": "New York", "NC": "North Carolina", "ND": "North Dakota", "OH": "Ohio", "OK": "Oklahoma", "OR": "Oregon", "PA": "Pennsylvania", "RI": "Rhode Island", "SC": "South Carolina", "SD": "South Dakota", "TN": "Tennessee", "TX": "Texas", "UT": "Utah", "VT": "Vermont", "VA": "Virginia", "WA": "Washington", "WV": "West Virginia", "WI": "Wisconsin", "WY": "Wyoming" };
  const perPage = 9;
  const [committees, setCommittees] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);
  const [totalInstances, setTotalInstances] = useState(0);
  const [searchQuery, setSearchQuery] = useState("");

  // Pagination
  function handlePage(num) {
    setCurrentPage(num)
  }

  let items = []
  for (let num = currentPage - 1; num <= currentPage + 1; num++) {
    if (num > 0 && num <= totalPages) {
      items.push(
        <Pagination.Item
          key={num}
          onClick={() => handlePage(num)}
          active={num === currentPage}>
          {num}
        </Pagination.Item>
      )
    }
  }

  // Filter hooks
  const [committeeType, setCommitteeType] = useState("");
  const handleCommitteeTypeFilter = (value) => {
    setCurrentPage(1);
    setCommitteeType(value);
  }
  const [committeeChair, setCommitteeChair] = useState("");
  const handleCommitteeChairFilter = (value) => {
    setCurrentPage(1);
    setCommitteeChair(value);
  }
  const [chairParty, setChairParty] = useState("");
  const handleChairPartyFilter = (value) => {
    setCurrentPage(1);
    setChairParty(value);
  }
  const [chairState, setChairState] = useState("");
  const handleChairStateFilter = (value) => {
    setCurrentPage(1);
    setChairState(value);
  }
  const [committeeChamber, setCommitteeChamber] = useState("");
  const handleCommitteeChamberFilter = (value) => {
    setCurrentPage(1);
    setCommitteeChamber(value);
  }

  useEffect(() => {
    const fetchData = async () => {
      var queryURL = `${API_URL}/get/committees?page=${currentPage}&per_page=${perPage}`;
      if (searchQuery !== "") {
        queryURL += `&search=${searchQuery}`;
      }
      if (committeeType !== "") {
        queryURL += `&committeeType=${committeeType}`;
      }
      if (committeeChair !== "") {
        queryURL += `&committeeChair=${committeeChair}`
      }
      if (chairParty !== "") {
        queryURL += `&chairParty=${chairParty}`
      }
      if (chairState !== "") {
        queryURL += `&chairState=${chairState}`
      }
      if (committeeChamber !== "") {
        queryURL += `&committeeChamber=${committeeChamber}`
      }
      let response = await axios.get(queryURL);
      setCommittees(response["data"]["news"])
      setTotalPages(Math.ceil(response["data"]["total"] / perPage))
      setTotalInstances(response["data"]["total"])
    };
    fetchData();
  }, [currentPage, perPage, searchQuery, committeeType, committeeChair, chairParty, chairState, committeeChamber]);

  const handleSearch = (event) => {
    setSearchQuery(event.target.value);
  }
  const handleSearchSubmit = (event) => {
    event.preventDefault();
  };
  function handleReset() {
    window.location.reload();
  }

  return (
    <Container className="mt-5 mb-4">
      {/* Input search Bar */}
      <Form onSubmit={handleSearchSubmit}>
        <FloatingLabel
          controlId="searchInput"
          label="Search for a committee!"
          className="mb-3">
          <Form.Control type="search" onChange={handleSearch} value={searchQuery} />
        </FloatingLabel>
      </Form>
      {/* Filtering */}
      <Form className="d-flex gap-4 justify-content-center pb-2 mt-4">
        <Filter title="Committee Type" items={["Main Committee", "Subcommittee"]} onChange={handleCommitteeTypeFilter}></Filter>
        <Filter title="Committee Chair" items={chair_list} scroll onChange={handleCommitteeChairFilter}></Filter>
        <Filter title="Chair Party" items={["Democratic Party", "Republican Party"]} onChange={handleChairPartyFilter}></Filter>
        <Filter title="Chair State" items={states} scroll onChange={handleChairStateFilter}></Filter>
        <Filter title="Committee Chamber" items={["House", "Senate", "Joint"]} onChange={handleCommitteeChamberFilter}></Filter>
        <Button variant="primary" onClick={handleReset}>Reset</Button>
      </Form>
      <Row className="mb-3 justify-content-center">
        {committees != null && committees.map((data) => {
          return (
            <Col>
              <CommitteesCard
                id={data.id}
                chamber={data.chamber}
                main_committee={data.main_committee}
                chair={data.chair}
                chair_party={data.chair_party}
                chair_state={states_dict[data.chair_state]}
                name={data.name}
                twitter={data.twitter}
                logo_url={data.image}
                searchQuery={searchQuery}>
              </CommitteesCard>
            </Col>
          )
        })}
      </Row>
      <Pagination className="justify-content-center mt-5">
        {currentPage > 2 && (
          <Pagination.First
            key={1}
            onClick={() => handlePage(1)}
            active={1 === currentPage}>
            1
          </Pagination.First>
        )}
        {currentPage > 3 && <Pagination.Ellipsis />}
        {items}
        {currentPage < totalPages - 2 && <Pagination.Ellipsis />}
        {currentPage < totalPages - 1 && (
          <Pagination.Last
            key={totalPages}
            onClick={() => handlePage(totalPages)}
            active={totalPages === currentPage}>
            {totalPages}
          </Pagination.Last>
        )}
      </Pagination>
      <div style={{ textAlign: "center" }}>Showing {currentPage === totalPages ? 
        (totalInstances === 0 ? 0 : (totalInstances % perPage === 0 ? perPage : totalInstances % perPage)) : 
        (totalInstances === 0 ? 0 : perPage)} of {totalInstances} committees</div>
    </Container>
  )
};


export default Committees;