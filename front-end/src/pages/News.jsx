import { useState, useEffect } from 'react';
import NewsCard from "../components/Cards/NewsCard"
import axios from "axios";
import { API_URL } from './URL';
import { Container, Form, FloatingLabel, Col, Row, Button, Pagination } from 'react-bootstrap'
import Filter from "../components/Filter"
import { news_publishers_list } from '../data/NewsPublishers';

function News() {
  const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
  const perPage = 9;
  const [news, setNews] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);
  const [totalInstances, setTotalInstances] = useState(0);
  const [searchQuery, setSearchQuery] = useState("");

  // Pagination
  function handlePage(num) {
    setCurrentPage(num)
  }

  function removeHTMLTags(htmlString) {
    const doc = new DOMParser().parseFromString(htmlString, 'text/html');
    return doc.body.textContent || '';
  }

  let items = []
  for (let num = currentPage - 1; num <= currentPage + 1; num++) {
    if (num > 0 && num <= totalPages) {
      items.push(
        <Pagination.Item
          key={num}
          onClick={() => handlePage(num)}
          active={num === currentPage}>
          {num}
        </Pagination.Item>
      )
    }
  }

  // Filter hooks
  const [publisher, setPublisher] = useState("");
  const handlePublisherFilter = (value) => {
    setCurrentPage(1);
    setPublisher(value);
  }
  const [title, setTitle] = useState("");
  const handleTitleFilter = (value) => {
    setCurrentPage(1);
    setTitle(value);
  }
  const [month, setMonth] = useState("");
  const handleMonthFilter = (value) => {
    setCurrentPage(1);
    setMonth(value);
  }
  const [date, setDate] = useState("");
  const handleDateFilter = (value) => {
    setCurrentPage(1);
    setDate(value);
  }
  const [author, setAuthor] = useState("");
  const handleAuthorFilter = (value) => {
    setCurrentPage(1);
    setAuthor(value);
  }

  useEffect(() => {
    const fetchData = async () => {
      var queryURL = `${API_URL}/get/news?page=${currentPage}&per_page=${perPage}`;
      if (searchQuery !== "") {
        queryURL += `&search=${searchQuery}`;
      }
      if (publisher !== "") {
        queryURL += `&publisher=${publisher}`;
      }
      if (month !== "") {
        queryURL += `&month=${month}`;
      }
      if (title !== "") {
        queryURL += `&title=${title}`;
      }
      if (date !== "") {
        queryURL += `&date=${date}`;
      }
      if (author !== "") {
        console.log("author is" + author)
        queryURL += `&author=${author}`;
      }
      let response = await axios.get(queryURL);
      setNews(response["data"]["news"])
      setTotalPages(Math.ceil(response["data"]["total"] / perPage))
      setTotalInstances(response["data"]["total"])
    };
    fetchData();
  }, [currentPage, perPage, searchQuery, publisher, month, title, date, author]);

  const handleSearch = (event) => {
    setSearchQuery(event.target.value);
  }
  const handleSearchSubmit = (event) => {
    event.preventDefault();
  };
  function handleReset() {
    window.location.reload();
  }

  return (
    <Container className="mt-5 mb-4">
      {/* Input search Bar */}
      <Form onSubmit={handleSearchSubmit}>
        <FloatingLabel
          controlId="searchInput"
          label="Search for a news article!"
          className="mb-3">
          <Form.Control type="search" onChange={handleSearch} value={searchQuery} />
        </FloatingLabel>
      </Form>
      {/* Filtering */}
      <Form className="d-flex gap-4 justify-content-center pb-2 mt-4">
        <Filter title="Publisher" items={news_publishers_list} scroll onChange={handlePublisherFilter}></Filter>
        <Filter title="Month Published" items={months} scroll onChange={handleMonthFilter}></Filter>
        <Filter title="Sort by Title" items={["Sort A-Z", "Sort Z-A"]} onChange={handleTitleFilter}></Filter>
        <Filter title="Sort by Date" items={["Ascending Date", "Descending Date"]} onChange={handleDateFilter}></Filter>
        <Filter title="Sort by Author" items={["Sort A-Z", "Sort Z-A"]} onChange={handleAuthorFilter}></Filter>
        <Button variant="primary" onClick={handleReset}>Reset</Button>
      </Form>
      <Row className="mb-3 justify-content-center">
        {news != null && news.map((data) => {
          return (
            <Col>
              <NewsCard
                id={data.id}
                title={data.title}
                author={data.author}
                publisher={data.publisher}
                description={removeHTMLTags(data.description)}
                image={data.image}
                date={data.date}
                month={data.month}
                searchQuery={searchQuery}>
              </NewsCard>
            </Col>
          )
        })}
      </Row>
      <Pagination className="justify-content-center mt-5">
        {currentPage > 2 && (
          <Pagination.First
            key={1}
            onClick={() => handlePage(1)}
            active={1 === currentPage}>
            1
          </Pagination.First>
        )}
        {currentPage > 3 && <Pagination.Ellipsis />}
        {items}
        {currentPage < totalPages - 2 && <Pagination.Ellipsis />}
        {currentPage < totalPages - 1 && (
          <Pagination.Last
            key={totalPages}
            onClick={() => handlePage(totalPages)}
            active={totalPages === currentPage}>
            {totalPages}
          </Pagination.Last>
        )}
      </Pagination>
      <div style={{ textAlign: "center" }}>Showing {currentPage === totalPages ? 
        (totalInstances === 0 ? 0 : (totalInstances % perPage === 0 ? perPage : totalInstances % perPage)) : 
        (totalInstances === 0 ? 0 : perPage)} of {totalInstances} news articles</div>
    </Container>
  )
};

export default News;