// const API_URL = "http://127.0.0.1:5000"; // testing back-end
const API_URL = "https://api.your-voice.me"; // deployed back-end

export { API_URL };
