import axios from "axios";
import { API_URL } from '../pages/URL';

export const getCommittee = async (id) => {
    const response = await axios.get(`${API_URL}/get/committees/${id}`)
    return response["data"]["news"]
}

export const getNews = async (id) => {
    const response = await axios.get(`${API_URL}/get/news/${id}`)
    return response["data"]["news"]
}

export const getPolitician = async (id) => {
    const response = await axios.get(`${API_URL}/get/politicians/${id}`)
    return response["data"]["news"]
}