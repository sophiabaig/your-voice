import APICard from "../components/Cards/APICard"
import HomeNavCard from "../components/Cards/HomeNavCard"
import NewsCard from "../components/Cards/NewsCard"
import SenatorCard from "../components/Cards/SenatorCard"
import TeamCard from "../components/Cards/TeamCard"
import ToolCard from "../components/Cards/ToolCard"
import About from "../pages/About"
import Home from "../pages/Home"
import News from "../pages/News"
import Senators from "../pages/Senators"
import NewsInstance from "../pages/NewsInstance"
import SenatorInstance from "../pages/Senators"
import Committees from "../pages/Committees"
import CommiteesInstance from "../pages/Committees"

import { BrowserRouter } from 'react-router-dom';

describe("Main Pages", () => {
    it("Render Home Page", () => {
        const tree = <BrowserRouter>renderer.create(<Home />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    })

    it("Render News Page", () => {
        const tree = <BrowserRouter>renderer.create(<News />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    })

    it("Render Senators Page", () => {
        const tree = <BrowserRouter>renderer.create(<Senators />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    })

    it("Render About Page", () => {
        const tree = <BrowserRouter>renderer.create(<About />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    })

    it("Render Committees Page", () => {
        const tree = <BrowserRouter>renderer.create(<Committees />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    })
})

describe("Render Instance Pages", () => {
    it("Render News Instance", () => {
        const tree = <BrowserRouter>renderer.create(<NewsInstance />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    })

    it("Render Senator Instance", () => {
        const tree = <BrowserRouter>renderer.create(<SenatorInstance />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    })

    it("Render Committee Instance", () => {
        const tree = <BrowserRouter>renderer.create(<CommiteesInstance />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    })
})

describe("Cards", () => {
    it("API Card", () => {
        const tree = <BrowserRouter>renderer.create(<APICard />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    })

    it("HomeNav Card", () => {
        const tree = <BrowserRouter>renderer.create(<HomeNavCard />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    })

    it("News Card", () => {
        const tree = <BrowserRouter>renderer.create(<NewsCard />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    })

    it("Senator Card", () => {
        const tree = <BrowserRouter>renderer.create(<SenatorCard />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    })

    it("Team Card", () => {
        const tree = <BrowserRouter>renderer.create(<TeamCard />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    })

    it("Tool Card", () => {
        const tree = <BrowserRouter>renderer.create(<ToolCard />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    })

    it("API Card Data", () => {
        const API = {
            image: "image",
            title: "title",
            description: "description",
            link: "link"
        }
        const tree = <BrowserRouter>renderer.create(<APICard img="image" title="title" description="description" link="link" />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    })

    it("HomeNav Card Data", () => {
        const card = {
            page: "page",
            img: "img",
            description: "description",
            link: "link"
        }
        const tree = <BrowserRouter>renderer.create(<APICard page={card.page} img={card.img} description={card.description} link={card.link} />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    })

    it("News Card Data", () => {
        const card = {
            title: "title",
            author: "author",
            publisher: "publisher",
            description: "description",
            image: "image",
            date: "date",
            month: "month",
            year: "year",
            politician: "politician",
            committee: "committee"
        }
        const tree = <BrowserRouter>renderer.create(<NewsCard title={card.title} author={card.author} image={card.image} description={card.description} date={card.date} />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    })

    it("Senator Card Data", () => {
        const card = {
            name: "name",
            position: "position",
            state: "state",
            role: "role"
        }
        const tree = <BrowserRouter>renderer.create(<SenatorCard name={card.name} position={card.position} state={card.state} role={card.description} date={card.date} />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    })

    it("Team Card Data", () => {
        const card = {
            name: "name",
            gitlab_username: "gitlab_username",
            role: "role",
            bio: "bio",
            commits: "commits"
        }
        const tree = <BrowserRouter>renderer.create(<TeamCard name={card.name} gitlab_username={card.gitlab_username} role={card.role} description={card.description} bio={card.bio} commits={card.commits} />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    })

    it("Tool Card Data", () => {
        const card = {
            img: "img",
            title: "title",
            description: "description",
            link: "link"
        }
        const tree = <BrowserRouter>renderer.create(<ToolCard img={card.img} title={card.title} description={card.description} link={card.link} />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    })
})