export const chair_list = [
    "Zoe Lofgren",
    "Collin C. Peterson",
    "Nita M. Lowey",
    "Adam Smith",
    "Robert C. Scott",
    "Frank Pallone",
    "Ted Deutch",
    "Maxine Waters",
    "Eliot L. Engel",
    "Bennie Thompson",
    "Ra\u00fal M. Grijalva",
    "Elijah E. Cummings",
    "Jim McGovern",
    "Eddie Bernice Johnson",
    "Nydia M. Vel\u00e1zquez",
    "John Yarmuth",
    "Jerrold Nadler",
    "Peter A. DeFazio",
    "Mark Takano",
    "Richard E. Neal",
    "Adam B. Schiff",
    "Kathy Castor",
    "Derek Kilmer",
    "Pat Roberts",
    "Richard C. Shelby",
    "James M. Inhofe",
    "Michael D. Crapo",
    "Roger Wicker",
    "Lisa Murkowski",
    "John Barrasso",
    "Charles E. Grassley",
    "Jim Risch",
    "Lamar Alexander",
    "Ron Johnson",
    "John Hoeven",
    "Roy Blunt",
    "Marco Rubio",
    "Michael B. Enzi",
    "Lindsey Graham",
    "Johnny Isakson",
    "Richard M. Burr",
    "Susan Collins",
    "John Cornyn",
    "Alcee L. Hastings"
]