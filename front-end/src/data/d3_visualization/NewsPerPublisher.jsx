import { Container, Col, Row } from 'react-bootstrap'
import React, { useEffect, useState } from "react";
import axios from "axios"
import { PieChart, Pie, Tooltip, ResponsiveContainer } from 'recharts';

function NewsPerPublisher() {
    const [loaded, setLoaded] = useState(false)
    const [posts, setPosts] = useState([])

    useEffect(() => {
        const fetchPosts = async () => {
            if (!loaded) {
                var query = 'https://api.your-voice.me/get/news'
                var response = await axios.get(query)

                var data = response.data.news

                console.log(data)
                var publisher_count = {};
                data.forEach(news => {
                    if (news["publisher"] in publisher_count) {
                        publisher_count[news['publisher']] += 1
                    }
                    else {
                        publisher_count[news['publisher']] = 1
                    }
                })

                var tempArray = []
                for (const [key, value] of Object.entries(publisher_count)) {
                    if (value >= 20) {
                        var count_dict = {
                            "name": key,
                            "count": value
                        }
                        tempArray.push(count_dict)
                    }
                }
                setPosts(tempArray)
                setLoaded(true)
            }
        }
        fetchPosts()
        console.log(posts)
    }, [loaded, posts])

    return (
        <Container>
            <Row style={{ width: "100%", height: 600 }}>
                <h3 className="p-5 text-center">Number of Articles by Each Publisher (20 minimum)</h3>
                <Col>
                    <ResponsiveContainer width="100%" height="100%">
                        <PieChart width={400} height={400}>
                            <Pie
                                dataKey="count"
                                isAnimationActive={false}
                                data={posts}
                                cx="50%"
                                cy="50%"
                                outerRadius={200}
                                fill="#abdfed"
                                label
                            />
                            <Tooltip />
                        </PieChart>
                    </ResponsiveContainer>
                </Col>
            </Row>
        </Container>
    )
}

export default NewsPerPublisher;