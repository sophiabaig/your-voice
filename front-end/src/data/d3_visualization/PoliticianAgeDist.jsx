import { Container, Col, Row } from 'react-bootstrap'
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ReferenceLine, ResponsiveContainer } from "recharts";
import React, { useState, useEffect } from 'react'
import axios from "axios"

function PoliticianAgeDist() {
    const [loaded, setLoaded] = useState(false)
    const [ages, setAges] = useState([])

    useEffect(() => {
        const fetchAges = async () => {
            if (!loaded) {
                var query = 'https://api.your-voice.me/get/politicians'
                var response = await axios.get(query)

                var data = response.data.news

                console.log(data)
                var age_count = {}
                age_count["21 - 30"] = 0
                age_count["31 - 40"] = 0
                age_count["41 - 50"] = 0
                age_count["51 - 60"] = 0
                age_count["61 - 70"] = 0
                age_count["71 - 80"] = 0
                age_count["81 - 90"] = 0

                data.forEach(news => {
                    if (news["age"] >= 21 && news["age"] <= 30) {
                        age_count["21 - 30"] += 1
                    }
                    if (news["age"] >= 31 && news["age"] <= 40) {
                        age_count["31 - 40"] += 1
                    }
                    if (news["age"] >= 41 && news["age"] <= 50) {
                        age_count["41 - 50"] += 1
                    }
                    if (news["age"] >= 51 && news["age"] <= 60) {
                        age_count["51 - 60"] += 1
                    }
                    if (news["age"] >= 61 && news["age"] <= 70) {
                        age_count["61 - 70"] += 1
                    }
                    if (news["age"] >= 71 && news["age"] <= 80) {
                        age_count["71 - 80"] += 1
                    }
                    if (news["age"] >= 81 && news["age"] <= 90) {
                        age_count["81 - 90"] += 1
                    }
                })

                var tempArray = []
                for (const [key, value] of Object.entries(age_count)) {
                    var count_dict = {
                        "category": key,
                        "count": value
                    }
                    tempArray.push(count_dict)
                }
                setAges(tempArray)
                setLoaded(true)
            }
        }
        fetchAges()
        console.log(ages)
    }, [loaded, ages])

    return (
        <Container fluid="md">
            <Row style={{ width: "100%", height: 600 }}>
                <h3 className="p-5 text-center">Politician Age Distribution</h3>
                <Col>
                    <ResponsiveContainer width="100%" height="100%">
                        <BarChart
                            width={500}
                            height={300}
                            data={ages}
                            margin={{
                                top: 5,
                                right: 30,
                                left: 20,
                                bottom: 5,
                            }}
                        >
                            <CartesianGrid strokeDasharray="3 3" />
                            <XAxis dataKey="category" />
                            <YAxis />
                            <ReferenceLine y={52919.29} stroke="#000" />
                            <Tooltip />
                            <Legend />
                            <Bar dataKey="count" fill="#1e81b0" />
                        </BarChart>
                    </ResponsiveContainer>
                </Col>
            </Row>
        </Container>
    )
}

export default PoliticianAgeDist;