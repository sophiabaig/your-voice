import { Container, Col, Row } from 'react-bootstrap'
import React, { useEffect, useState } from "react";
import axios from "axios"
import { PieChart, Pie, Tooltip, ResponsiveContainer } from 'recharts';

function CommitteesParty() {
    const [loaded, setLodaed] = useState(false)
    const [parties, setParties] = useState([])

    useEffect(() => {
        const fetchParties = async() => {
            if (!loaded) {
                var query = 'https://api.your-voice.me/get/committees'
                var response = await axios.get(query)

                var data = response.data.news

                console.log(data)
                var party_count = {}
                data.forEach(parties => {
                    if(parties["chair_party"] in party_count) {
                        party_count[parties["chair_party"]] += 1
                    }
                    else {
                        party_count[parties["chair_party"]] = 1
                    }
                })

                var tempArray = []
                for (const [key, value] of Object.entries(party_count)) {
                    var count_dict = {
                        "name": key,
                        "count": value
                    }
                    tempArray.push(count_dict)
                }
                setParties(tempArray)
                setLodaed(true)
            }
        }
        fetchParties()
        console.log(parties)
    }, [loaded, parties])

    return (
        <Container>
            <Row style={{ width: "100%", height: 600 }}>
                <h3 className="p-5 text-center">Chair Party Count for Committees</h3>
                <Col>
                    <ResponsiveContainer width="100%" height="100%">
                        <PieChart width={400} height={400}>
                            <Pie
                                dataKey="count"
                                isAnimationActive={false}
                                data={parties}
                                cx="50%"
                                cy="50%"
                                outerRadius={200}
                                fill="#abdfed"
                                label
                            />
                            <Tooltip />
                        </PieChart>
                    </ResponsiveContainer>
                </Col>
            </Row>
        </Container>
    )
}

export default CommitteesParty