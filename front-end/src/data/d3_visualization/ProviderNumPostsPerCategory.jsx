import { Container, Col, Row } from 'react-bootstrap'
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ReferenceLine, ResponsiveContainer } from "recharts";
import React, { useState, useEffect } from 'react'
import axios from "axios"

function Provider_NumPostsPerCategory() {
    const [loaded, setLoaded] = useState(false)
    const [posts, setPosts] = useState([])

    useEffect(() => {
        const fetchPosts = async () => {
            if (!loaded) {
                var query = "https://api.ecowiki.me/articles"
                var response = await axios.get(query)

                var data = response.data.data

                console.log(data)

                var cat_count = {}
                data.forEach(posts => {
                    if (posts["category"] in cat_count) {
                        cat_count[posts["category"]] += 1
                    }
                    else {
                        cat_count[posts["category"]] = 1
                    }
                })

                var tempArray = []
                var post_count = 0
                var key_count = 0
                for (const [key, value] of Object.entries(cat_count)) {
                    if (value >= 20) {
                        post_count += value
                        key_count += 1
                        var count_dict = {
                            "name": key,
                            "count": value
                        }
                        tempArray.push(count_dict)
                    }
                }
                console.log(post_count / key_count)
                setPosts(tempArray)
                setLoaded(true)
            }
        }
        fetchPosts()
        console.log(posts)
    }, [loaded, posts])

    return (
        <Container fluid="md">
            <Row style={{ width: "100%", height: 600 }}>
                <h3 className="p-5 text-center">Number of Posts Per Category (20 minimum)</h3>
                <Col>
                    <ResponsiveContainer width="100%" height="100%">
                        <BarChart
                            width={500}
                            height={300}
                            data={posts}
                            margin={{
                                top: 5,
                                right: 30,
                                left: 20,
                                bottom: 5,
                            }}
                        >
                            <CartesianGrid strokeDasharray="3 3" />
                            <XAxis dataKey="name" />
                            <YAxis />
                            <ReferenceLine y={40.5} stroke="#000" />
                            <Tooltip />
                            <Legend />
                            <Bar dataKey="count" fill="#abdfed" />
                        </BarChart>
                    </ResponsiveContainer>
                </Col>
            </Row>
        </Container>
    )
}

export default Provider_NumPostsPerCategory