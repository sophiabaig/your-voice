import { Container, Col, Row } from 'react-bootstrap'
import React, { useEffect, useState } from "react";
import axios from "axios"
import {  Radar, RadarChart, PolarGrid, PolarAngleAxis, PolarRadiusAxis, ResponsiveContainer } from 'recharts';


function Provider_FrequencyOfTags() {
    const [loaded, setLoaded] = useState(false)
    const [tags, setTags] = useState([])

    useEffect(() => {
        const fetchTags = async () => {
            if (!loaded) {
                var query = "https://api.ecowiki.me/posts"
                var response = await axios.get(query)

                var data = response.data.data

                console.log(data)
                var tag_count = {}
                data.forEach(tags => {
                    if (tags["tag"] in tag_count) {
                        tag_count[tags["tag"]] += 1
                    }
                    else {
                        tag_count[tags["tag"]] = 1
                    }
                })

                var tempArray = []
                for (const [key, value] of Object.entries(tag_count)) {
                    var count_dict = {
                        "name": key,
                        "count": value
                    }
                    tempArray.push(count_dict)
                }
                setTags(tempArray)
                setLoaded(true)
            }
        }
        fetchTags()
        console.log(tags)
    }, [loaded, tags])

    return (
        <Container fluid="md">
            <Row style={{ width: "100%", height: 600 }}>
                <h3 className="p-5 text-center">Tag Count of All the Posts</h3>
                <Col>
                    <ResponsiveContainer width="100%" height="100%">
                        <RadarChart cx="50%" cy="50%" outerRadius="80%" data={tags}>
                            <PolarGrid stroke="black"/>
                            <PolarAngleAxis stroke="black" dataKey="name" />
                            <PolarRadiusAxis tick={{ stroke: "black" }}/>
                            <Radar name="category" dataKey="count" stroke="#1e81b0" fill="#1e81b0" fillOpacity={1} />
                        </RadarChart>
                    </ResponsiveContainer>
                </Col>
            </Row>
        </Container>
    );

}

export default Provider_FrequencyOfTags
