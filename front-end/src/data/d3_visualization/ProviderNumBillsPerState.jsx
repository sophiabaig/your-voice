import { Container, Col, Row } from 'react-bootstrap'
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ReferenceLine, ResponsiveContainer } from "recharts";
import React, { useState, useEffect } from 'react'
import axios from "axios"

function Provider_NumBillsPerState() {
    const [loaded, setLoaded] = useState(false)
    const [bills, setBills] = useState([])

    useEffect(() => {
        const fetchBills = async () => {
            if (!loaded) {
                var query = "https://api.ecowiki.me/bills"
                var response = await axios.get(query)

                var data = response.data.data

                console.log(data)

                var bill_count = {}
                data.forEach(bills => {
                    if (bills["sponsor_state"] in bill_count) {
                        bill_count[bills["sponsor_state"]] += 1
                    }
                    else {
                        bill_count[bills["sponsor_state"]] = 1
                    }
                })

                var sorted_states = Object.keys(bill_count).sort()

                var sorted_bill_count = {}
                for (let key of sorted_states) {
                    sorted_bill_count[key] = bill_count[key]
                }

                var tempArray = []
                var bill_count = 0
                var state_count = 0
                for (const [key, value] of Object.entries(sorted_bill_count)) {
                    bill_count += value
                    state_count += 1
                    var count_dict = {
                        "name": key,
                        "count": value
                    }
                    tempArray.push(count_dict)
                }
                console.log(bill_count / state_count)
                setBills(tempArray)
                setLoaded(true)
            }
        }
        fetchBills()
        console.log(bills)
    }, [loaded, bills])

    return (
        <Container fluid="md">
            <Row style={{ width: "100%", height: 600 }}>
                <h3 className="p-5 text-center">Number of Bills Sponsored by Each State</h3>
                <Col>
                    <ResponsiveContainer width="100%" height="100%">
                        <BarChart
                            width={500}
                            height={300}
                            data={bills}
                            margin={{
                                top: 5,
                                right: 30,
                                left: 20,
                                bottom: 5,
                            }}
                        >
                            <CartesianGrid strokeDasharray="3 3" />
                            <XAxis dataKey="name" />
                            <YAxis />
                            <ReferenceLine y={16.32075471698113} stroke="#000" />
                            <Tooltip />
                            <Legend />
                            <Bar dataKey="count" fill="#abdfed" />
                        </BarChart>
                    </ResponsiveContainer>
                </Col>
            </Row>
        </Container>
    )
}

export default Provider_NumBillsPerState