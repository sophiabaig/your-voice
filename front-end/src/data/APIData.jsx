// This is our manually inputed data for our API-toolchain

export const api_list = [
    {
        title: "Gitlab API",
        image: "https://cdn4.iconfinder.com/data/icons/logos-and-brands/512/144_Gitlab_logo_logos-512.png",
        description: "Gitlab API for fetching repository data",
        link: "https://docs.gitlab.com/ee/api/"
    },
    {
        title: "ProPublica API",
        image: "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcSFDWqzu-Rzxv4xvST-wgZM5V_Kqx67QJZOxUfoM_DS3Si0CYuY",
        description: "ProPublica API to get committees and subcommittees",
        link: "https://projects.propublica.org/api-docs/congress-api/committees/"
    },
    {
        title: "Google Civic Information API",
        image: "https://static.vecteezy.com/system/resources/previews/009/469/630/original/google-logo-isolated-editorial-icon-free-vector.jpg",
        description: "Google Civic Information API to get politicians",
        link: "https://developers.google.com/civic-information"
    },
    {
        title: "News API",
        image: "https://newsapi.org/images/n-logo-border.png",
        description: "News API used to find real time news articles",
        link: "https://newsapi.org/docs"
    },
    {
        title: "Committee Members API",
        image: "https://github.githubassets.com/images/modules/logos_page/GitHub-Mark.png",
        description: "GitHub repo used to get JSON file of current committee members",
        link: "https://github.com/unitedstates/congress-legislators"
    },
    {
        title: "Bing Image Search API",
        image: "https://edgestatic-ehf9gbe6gfdfdec4.z01.azurefd.net/shared/edgeweb/img/bing-icon.54c50c8.png",
        description: "Bing Image Search API to retrieve photos of House Representatives",
        link: "https://learn.microsoft.com/en-us/bing/search-apis/bing-image-search/overview"
    }
]