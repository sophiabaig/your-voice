// This file is rougly inspired by the GeoJobs Project. https://gitlab.com/sarthaksirotiya/cs373-idb/-/blob/main/front-end/src/static/TeamInfo.jsx

import sheryl_photo from '../images/profilepictures/sheryl.jpg'
import dayou_photo from '../images/profilepictures/dayou.jpeg'
import sophia_photo from '../images/profilepictures/sophia.png'

export const teamData = [
    {
        id: 0,
        name: "Sophia Baig",
        name2: "",
        gitlab_username: "sophiabaig",
        email: "sophiabaig@utexas.edu",
        image: sophia_photo,
        role: "Full Stack",
        bio: "I'm a junior from Austin, TX majoring in Computer Science and minoring in Entrepreneurship. I like going to coffee shops, going to national parks, and being creative.",
        commits: 0,
        issues: 0,
        unit_tests: 0,
    },
    {
        id: 1,
        name: "Alejandro Arias Diaz",
        name2: "Alejandro Arias",
        gitlab_username: "jandroizer",
        email: "alejandroariasdiaz09@gmail.com",
        image: "https://miro.medium.com/v2/resize:fit:1400/format:webp/1*YAM1wHSDwKRodKJ1Wprv-A.jpeg",
        role: "Full Stack",
        bio: "Junior Computer Science major at UT. Outisde of academics, I enjoy rock climbing, reading, and playing video games.",
        commits: 0,
        issues: 0,
        unit_tests: 43,
    },
    {
        id: 2,
        name: "Cristian Astorga",
        name2: "CAstorga23",
        gitlab_username: "CAstorga23",
        email: "Critian.Astorga2023@utexas.edu",
        image: "https://miro.medium.com/v2/resize:fit:1400/format:webp/1*kO-FQ1SvcEI5sYHHmtTgqQ.jpeg",
        role: "Full Stack, AWS",
        bio: "I'm a senior Computer Science major at UT. I like camping, reading, sleeping, and playing video games with my friends.",
        commits: 0,
        issues: 0,
        unit_tests: 0,
    },
    {
        id: 3,
        name: "Sheryl Dsouza",
        name2: "Sheryl",
        gitlab_username: "sheryl-dsouza",
        email: "sheryl.dsouza@utexas.edu",
        image: sheryl_photo,
        role: "Full Stack",
        bio: "I'm a junior Computer Science and Psychology double major pursuing a certificate in Design Strategies at the University of Texas at Austin. I like painting, playing tennis, and exploring new coffee spots in Austin.",
        commits: 0,
        issues: 0,
        unit_tests: 0,
    },
    {
        id: 4,
        name: "Dayou Ren",
        name2: "Peter",
        gitlab_username: "dayou919",
        email: "peter.ren@utexas.edu",
        image: dayou_photo,
        role: "Full Stack",
        bio: "I am a senior Computer Science and Math double major.",
        commits: 0,
        issues: 0,
        unit_tests: 7,
    },
]





