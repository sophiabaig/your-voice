// This file is inspired by the GeoJobs Project. https://gitlab.com/sarthaksirotiya/cs373-idb/-/blob/main/front-end/src/components/FilterDropdown.jsx

import { useState } from "react"
import { Container, Dropdown, DropdownButton } from 'react-bootstrap'

function Filter(props) {
  const title = props.title
  const items = props.items
  const scroll = props.scroll
  const onChange = props.onChange
  const [filterBy, setFilterBy] = useState(title)

  const handleClick = (value) => {
    setFilterBy(value);
    onChange(value);
  };

  return (
    <DropdownButton className="me-2" variant="secondary" title={filterBy} menuVariant="dark">
      <Container style={scroll ? { height: "20rem", overflowY: "scroll" } : {}}>
        {items.map((item) => {
          return (
            <Dropdown.Item onClick={() => handleClick(item)}>{item}</Dropdown.Item>
          );
        })}
      </Container>
    </DropdownButton>
  );
};

export default Filter;