import { useState } from 'react';
import { Card, Container, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTwitter, faFacebook } from '@fortawesome/free-brands-svg-icons'
import placeholder from '../../images/placeholders/politician.jpeg';

function SenatorCard(props) {
  const id = props.id
  const name = props.name
  const position = props.position
  const state = props.state
  const party = props.party
  const image = props.image
  const facebook = props.facebook
  const twitter = props.twitter
  const searchQuery = props.searchQuery
  const terms = props.terms
  const age = props.age
  const district = props.district

  // for icon hover colors
  const [overF, setOverF] = useState(false);
  const [overT, setOverT] = useState(false);

  const highlightSearchQuery = (text) => {
    if (!text) {
      return ''
    }
    if (searchQuery) {
      const regex = new RegExp(`(${searchQuery})`, 'gi');
      const parts = text.split(regex);
      return parts.map((part, index) => {
        if (regex.test(part)) {
          return <mark key={index}>{part}</mark>;
        } else {
          return part;
        }
      });
    }
    return text;
  };

  const addPlaceholder = (event) => {
    event.currentTarget.src = placeholder;
  };

  return (
    <Container>
      <Card className="mt-5" id={id} style={{ width: '24rem', display: 'flex' }}>
        <Card.Img variant="top" src={image ? image : placeholder} onError={addPlaceholder}
          style={{ objectFit: 'cover', height: '50vh' }}></Card.Img>
        <Card.Body>
          <Card.Title><b>{highlightSearchQuery(name)}</b></Card.Title>
          <Card.Text>{highlightSearchQuery(position)}</Card.Text>
          <Card.Text>
            {state && <div><b>State: </b>{highlightSearchQuery(state)}{district && <span>, District {highlightSearchQuery(district)}</span>}<br /></div>}
            {party && <div><b>Party: </b>{highlightSearchQuery(party)} <br></br></div>}
            {age && <div><b>Age: </b>{age}<br></br></div>}
            {terms && <div><b># of Years in Office: </b>{terms}<br></br></div>}
          </Card.Text>
          <Link to={`/politicians/${id}`}><Button variant="dark" >Politician Details</Button></Link>
          {facebook && <span style={{ position: "absolute", bottom: 18, right: 57 }} onMouseOver={() => setOverF(true)} onMouseLeave={() => setOverF(false)}>
            <a href={"https://www.facebook.com/" + facebook} target="blank">
              <FontAwesomeIcon icon={faFacebook} size="2xl" style={overF ? { color: "#293e6a" } : { color: "#3b5998" }} />
            </a>
          </span>}
          {twitter && <span style={{ position: "absolute", bottom: 18, right: 15 }} onMouseOver={() => setOverT(true)} onMouseLeave={() => setOverT(false)}>
            <a href={"https://twitter.com/" + twitter} target="blank">
              <FontAwesomeIcon icon={faTwitter} size="2xl" style={overT ? { color: "#0078a7" } : { color: "#00acee" }} />
            </a>
          </span>}
        </Card.Body>
      </Card>
    </Container>
  );
}

export default SenatorCard