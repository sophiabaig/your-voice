import { Card, Container } from 'react-bootstrap'
import { Link } from "react-router-dom"

function ToolCard(props) {
    const img = props.image
    const title = props.title
    const description = props.description
    const link = props.link

    return (
        <Container>
            <Card className="mt-3" style={{ width: '24rem', display: "flex" }}>
                <Card.Img variant="top" src={img} style={{ objectFit: 'cover', height: '55vh' }}></Card.Img>
                <Card.Body>
                    <Card.Title><b>{title}</b></Card.Title>
                    <Card.Text>{description}</Card.Text>
                    <Link to={link}>More Info</Link>
                </Card.Body>
            </Card>
        </Container>
    );
}

export default ToolCard;