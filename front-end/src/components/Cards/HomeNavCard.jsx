import { Card, Button } from 'react-bootstrap'

function HomeNavCard(props) {
    const title = props.title
    const description = props.description
    const img = props.img
    const link = props.link

    return (
        <Card>
            <Card.Img variant="top" src={img} width="100%"></Card.Img>
            <Card.Body>
                <Card.Title>{title}</Card.Title>
                <Card.Text>{description}</Card.Text>
                <Button variant="dark" href={link}>More Info</Button>
            </Card.Body>
        </Card>
    );
}

export default HomeNavCard;