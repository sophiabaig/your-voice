import { Card, Container, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom';
import placeholder from '../../images/placeholders/news.jpeg';

function NewsCard(props) {
  const id = props.id
  const title = props.title
  const author = props.author
  const publisher = props.publisher
  const description = props.description
  const image = props.image
  const month = props.month
  const searchQuery = props.searchQuery
  const tempDate = new Date(props.date);
  const formattedDate = tempDate.toLocaleDateString("en-US", {
    month: "long",
    day: "numeric",
    year: "numeric"
  });

  const highlightSearchQuery = (text) => {
    if (!text) {
      return ''
    }
    if (searchQuery) {
      const regex = new RegExp(`(${searchQuery})`, 'gi');
      const parts = text.split(regex);
      return parts.map((part, index) => {
        if (regex.test(part)) {
          return <mark key={index}>{part}</mark>;
        } else {
          return part;
        }
      });
    }
    return text;
  };

  const addPlaceholder = (event) => {
    event.currentTarget.src = placeholder;
  };

  return (
    <Container>
      <Card className="mt-5" id={id} style={{ width: '24rem', display: 'flex' }}>
        <Card.Img variant="top" src={image ? image : placeholder} onError={addPlaceholder}
          style={{ objectFit: 'cover', height: '30vh' }}></Card.Img>
        <Card.Body>
          <Card.Title><b>{highlightSearchQuery(title)}</b></Card.Title>
          <Card.Text>{highlightSearchQuery(description)}</Card.Text>
          <Card.Text>
            {publisher && <div><b>Publisher: </b>{highlightSearchQuery(publisher)} <br></br></div>}
            {author && <div><b>Author: </b>{highlightSearchQuery(author)} <br></br></div>}
            {month && <div><b>Month Published: </b>{highlightSearchQuery(month)} <br></br></div>}
            {formattedDate && <div><b>Date: </b>{formattedDate}<br></br></div>}
          </Card.Text>
          <Link to={`/news/${id}`}><Button variant="dark">News Details</Button></Link>
        </Card.Body>
      </Card>
    </Container>
  );
}

export default NewsCard