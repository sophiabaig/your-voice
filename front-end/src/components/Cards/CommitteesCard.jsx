import { useState } from 'react';
import { Card, Container, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTwitter } from '@fortawesome/free-brands-svg-icons'
import placeholder from '../../images/placeholders/committees.png';

function CommitteesCard(props) {
  const id = props.id
  const chamber = props.chamber
  const main_committee = props.main_committee
  const chair = props.chair
  const chair_party = props.chair_party
  const chair_state = props.chair_state
  const name = props.name
  const twitter = props.twitter
  const logo_url = props.logo_url
  const searchQuery = props.searchQuery

  // for icon hover colors
  const [overT, setOverT] = useState(false);

  const highlightSearchQuery = (text) => {
    if (!text) {
      return ''
    }
    if (searchQuery) {
      const regex = new RegExp(`(${searchQuery})`, 'gi');
      const parts = text.split(regex);
      return parts.map((part, index) => {
        if (regex.test(part)) {
          return <mark key={index}>{part}</mark>;
        } else {
          return part;
        }
      });
    }
    return text;
  };

  const addPlaceholder = (event) => {
    event.currentTarget.src = placeholder;
  };

  return (
    <Container>
      <Card className="mt-5" id={id} style={{ width: '24rem', display: 'flex' }}>
        <Card.Img variant="top" src={logo_url ? logo_url : placeholder} onError={addPlaceholder} style={{ objectFit: 'contain', height: '10rem', backgroundColor: '#D3D3D3' }}></Card.Img>
        <Card.Body>
          {main_committee ? (
            <Card.Title><b>Subcommittee on {highlightSearchQuery(name)}</b></Card.Title>
          ) : (
            <Card.Title><b>{highlightSearchQuery(name)}</b></Card.Title>
          )}
          <Card.Text>
            {chamber && <div><b>Chamber: </b>{highlightSearchQuery(chamber)} <br></br></div>}
            {main_committee ? (<div><b>Committee Type: </b>Subcommittee</div>) : (<div><b>Committee Type: </b>Main Committee</div>)}
            {main_committee ? (<div><b>Main Committee: </b>{highlightSearchQuery(main_committee)}</div>) : null}
            {chair && <div><b>Chair: </b>{highlightSearchQuery(chair)} <br></br></div>}
            {chair_party && <div><b>Chair Party: </b>{highlightSearchQuery(chair_party)} <br></br></div>}
            {chair_state && <div><b>Chair State: </b>{highlightSearchQuery(chair_state)} <br></br></div>}
          </Card.Text>
          <Link to={`/committees/${id}`}><Button variant="dark">Committee Details</Button></Link>
          {twitter && <span style={{ position: "absolute", bottom: 18, right: 15 }} onMouseOver={() => setOverT(true)} onMouseLeave={() => setOverT(false)}>
            <a href={"https://twitter.com/" + twitter} target = "blank">
              <FontAwesomeIcon icon={faTwitter} size="2xl" style={overT ? { color: "#0078a7" } : { color: "#00acee" }} />
            </a>
          </span>}
        </Card.Body>
      </Card>
    </Container>
  );
}

export default CommitteesCard