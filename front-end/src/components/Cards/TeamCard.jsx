// This file is rougly inspired by the GeoJobs Project. https://gitlab.com/sarthaksirotiya/cs373-idb/-/blob/main/front-end/src/components/Cards/DeveloperCard.jsx

import { Card, Container } from 'react-bootstrap'

function TeamCard(props) {
    const name = props.name
    const gitlab_username = props.gitlab_username
    const image = props.image
    const role = props.role
    const bio = props.bio
    const commits = props.commits
    const issues = props.issues
    const unit_tests = props.unit_tests

    return (
        <Container>
            <Card className="mt-3" style={{ width: '24rem', display: 'flex' }}>
                <Card.Img variant="top" src={image} style={{ objectFit: 'cover', height: '55vh' }}></Card.Img>
                <Card.Body>
                    <Card.Title><b>{name}</b></Card.Title>
                    <Card.Text>
                        <b>GitLab Username: </b>@{gitlab_username}<br></br>
                        <b>Role: </b>{role}<br></br>
                    </Card.Text>
                    <Card.Text>{bio}</Card.Text>
                </Card.Body>
                <Card.Footer>
                    <Card.Text>
                        <b>Commits: </b>{commits}<br></br>
                        <b>Issues: </b>{issues}<br></br>
                        <b>Unit Tests: </b>{unit_tests}<br></br>
                    </Card.Text>
                </Card.Footer>
            </Card>
        </Container>
    );
}

export default TeamCard
