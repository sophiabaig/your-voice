import { Container, Nav, Navbar } from 'react-bootstrap';
import yourVoiceLogo from '../images/yourvoicelogo.svg';
import { useLocation } from 'react-router-dom';

function GlobalNavbar() {
  const location = useLocation();

  return (
    <Navbar bg="dark" variant="dark">
      <Container>
        <Navbar.Brand href="/">
          <img
            alt=""
            src={yourVoiceLogo}
            width="30"
            height="30"
            className="d-inline-block align-top"
            style={{ marginRight: "10px" }}
          />YourVoice</Navbar.Brand>
        <Nav className="me-auto">
          <Nav.Link href="/" active={location.pathname === '/'}>Home</Nav.Link>
          <Nav.Link href="/about" active={location.pathname.startsWith('/about')}>About</Nav.Link>
          <Nav.Link href="/politicians" active={location.pathname.startsWith('/politicians')}>Politicians</Nav.Link>
          <Nav.Link href="/committees" active={location.pathname.startsWith('/committees')}>Committees</Nav.Link>
          <Nav.Link href="/news" active={location.pathname.startsWith('/news')}>News</Nav.Link>
          <Nav.Link href="/visualizations" active={location.pathname.startsWith('/visualizations')}>Visualizations</Nav.Link>
        </Nav>
      </Container>
    </Navbar>
  );
}

export default GlobalNavbar;