// This file is inspired by the GeoJobs Project. https://gitlab.com/sarthaksirotiya/cs373-idb/-/blob/main/front-end/src/components/RangeSlider.jsx

import { useState } from "react"
import Slider from "@mui/material/Slider"

function RangeSlider(props) {
    const min = props.min
    const max = props.max
    const discrete = props.discrete
    const onChange = props.onChange
    const [value, setValue] = useState([min, max])

    const handleChange = (event, value) => {
        setValue(value);
        onChange(value);
    };

    return (
        <Slider
            value={value}
            onChange={handleChange}
            valueLabelDisplay="on"
            marks={discrete}
            min={min}
            max={max}>
        </Slider>
    );
};

export default RangeSlider;