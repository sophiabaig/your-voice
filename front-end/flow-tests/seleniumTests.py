import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.service import Service

URL = "https://www.your-voice.me/"

class Test(unittest.TestCase):
    # Ripped the @classmethod functions from geojobs
    # at this link: https://gitlab.com/sarthaksirotiya/cs373-idb/-/blob/main/front-end/flow_tests/splashTests.py

    @classmethod
    def setUpClass(self) -> None:
        options = webdriver.ChromeOptions()
        options.add_experimental_option('excludeSwitches', ['enable-logging'])
        options.add_argument("--headless")
        options.add_argument("--no-sandbox")
        options.add_argument("--disable-dev-shm-usage")
        options.add_argument("--window-size=1440, 900")
        chrome_prefs = {}
        options.experimental_options["prefs"] = chrome_prefs
        self.driver = webdriver.Chrome(options = options, service = Service(ChromeDriverManager().install()))
        self.driver.get(URL)
        #self.driver.maximize_window()

    @classmethod
    def tearDownClass(self):
        self.driver.quit()

    @classmethod
    def ensureCanClick(self, elementCSS):
        WebDriverWait(self.driver, 10).until(
            EC.visibility_of_element_located((By.CSS_SELECTOR, elementCSS))
        )
        return WebDriverWait(self.driver, 5).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, elementCSS))
        )
    
   
    def testTitle(self):
        title = self.driver.title
        self.assertEqual(title, "YourVoice")

    
    def testNavLinkHome(self):
        element = self.driver.find_elements(By.LINK_TEXT, "Home")[0]
        updatedURL = element.get_attribute("href")
        self.assertEqual(updatedURL, URL)
    
    
    def testAboutLink(self):
        element = self.driver.find_elements(By.LINK_TEXT, "About")[0]
        updatedURL = element.get_attribute("href")
        self.assertEqual(updatedURL, URL + "/about")

   
    def testNewsLink(self):
        element = self.driver.find_elements(By.LINK_TEXT, "News")[0]
        updatedURL = element.get_attribute("href")
        self.assertEqual(updatedURL, URL + "/news")
    
    
    def testSenatorsLink(self):
        element = self.driver.find_elements(By.LINK_TEXT, "Senators")[0]
        updatedURL = element.get_attribute("href")
        self.assertEqual(updatedURL, URL + "/senators")

    def test_About(self):
        try:
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CLASS_NAME, 'navbar-brand')))
            element = self.driver.find_element(By.CLASS_NAME, 'navbar-brand')
            element.click()
        except Exception as ex:
            print("Couldn't find navbar brand: " + str(ex))

        try:
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CLASS_NAME, 'navbar-toggler')))
            element = self.driver.find_element(By.CLASS_NAME, 'navbar-toggler')
            element.click()
        except Exception as ex:
            print("Couldn't find navbar: " + str(ex))

        try:
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div/nav/div/div/div/a[2]')))
            element = self.driver.find_element(By.XPATH, '//*[@id="root"]/div/nav/div/div/div/a[2]')
            element.click()
        except Exception as ex:
         print("Couldn't find About link: " + str(ex))

        self.assertEqual(self.driver.current_url, URL + "about")

    def test_about(self):
        self.driver.get(URL + "about")
        self.assertEqual(self.driver.current_url, URL + "about")

    def test_SplashNewsLink(self):
        # Wait for the navbar-brand to load, then click to go back to the home screen
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CLASS_NAME, 'navbar-brand')))
        element = self.driver.find_element(By.CLASS_NAME, 'navbar-brand')
        element.click()
        # Wait for splash card to load, then click
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div/div/div[2]/div/div[1]/div/div/a')))
        element = self.driver.find_element(By.XPATH, '//*[@id="root"]/div/div/div[2]/div/div[1]/div/div/a')
        self.driver.execute_script("arguments[0].click();", element)
        self.assertEqual(self.driver.current_url, URL + "news")

    def test_SplashSenatorsLink(self):
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CLASS_NAME, 'navbar-brand')))
        element = self.driver.find_element(By.CLASS_NAME, 'navbar-brand')
        element.click()

        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div/div/div[2]/div/div[2]/div/div/a')))
        element = self.driver.find_element(By.XPATH, '//*[@id="root"]/div/div/div[2]/div/div[2]/div/div/a')
        self.driver.execute_script("arguments[0].click();", element)

        self.assertEqual(self.driver.current_url, URL + "senators")

    def test_Brand(self):
        element = self.driver.find_element(By.CLASS_NAME, 'navbar-brand')
        element.click()
        self.assertEqual(self.driver.current_url, URL)

    def test_Senators(self):
        try:
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CLASS_NAME, 'navbar-brand')))
            element = self.driver.find_element(By.CLASS_NAME, 'navbar-brand')
            element.click()
        except Exception as ex:
            print("Couldn't find navbar brand: " + str(ex))

        try:
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CLASS_NAME, 'navbar-toggler')))
            element = self.driver.find_element(By.CLASS_NAME, 'navbar-toggler')
            element.click()
        except Exception as ex:
            print("Couldn't find navbar: " + str(ex))

        try:
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div/nav/div/div/div/a[5]')))
            element = self.driver.find_element(By.XPATH, '//*[@id="root"]/div/nav/div/div/div/a[5]')
            element.click()
        except Exception as ex:
            print("Couldn't find About link: " + str(ex))

        self.assertEqual(self.driver.current_url, URL + "senators")

    def test_Committees(self):
        try:
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CLASS_NAME, 'navbar-brand')))
            element = self.driver.find_element(By.CLASS_NAME, 'navbar-brand')
            element.click()
        except Exception as ex:
            print("Couldn't find navbar brand: " + str(ex))

        try:
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CLASS_NAME, 'navbar-toggler')))
            element = self.driver.find_element(By.CLASS_NAME, 'navbar-toggler')
            element.click()
        except Exception as ex:
            print("Couldn't find navbar: " + str(ex))

        try:
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div/nav/div/div/div/a[5]')))
            element = self.driver.find_element(By.XPATH, '//*[@id="root"]/div/nav/div/div/div/a[5]')
            element.click()
        except Exception as ex:
            print("Couldn't find About link: " + str(ex))

        self.assertEqual(self.driver.current_url, URL + "committees")
    
    def test_News(self):
        try:
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CLASS_NAME, 'navbar-brand')))
            element = self.driver.find_element(By.CLASS_NAME, 'navbar-brand')
            element.click()
        except Exception as ex:
            print("Couldn't find navbar brand: " + str(ex))

        try:
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CLASS_NAME, 'navbar-toggler')))
            element = self.driver.find_element(By.CLASS_NAME, 'navbar-toggler')
            element.click()
        except Exception as ex:
            print("Couldn't find navbar: " + str(ex))

        try:
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div/nav/div/div/div/a[5]')))
            element = self.driver.find_element(By.XPATH, '//*[@id="root"]/div/nav/div/div/div/a[5]')
            element.click()
        except Exception as ex:
            print("Couldn't find About link: " + str(ex))

        self.assertEqual(self.driver.current_url, URL + "news")

if __name__ == '__main__':
    unittest.main()
