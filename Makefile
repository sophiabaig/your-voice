# get git config
config:
	git config -l

# get git log
Your-Voice.log.txt:
	git log > Your-Voice.log.txt

clean:
	rm -f *.tmp

# docker for front end: runs website on local 3000 port
frontend-docker:
	docker run -dp 3000:3000 front-end

#build frontend
build-frontend :
	docker build -t front-end front-end/

#build backend
build-backend :
	docker build -t your-voice-backend -f Dockerfile .

# Docker for running the backend as a developer on local machine
backend-dev-docker:
	docker run --rm -it -p 5000:8000 your-voice-backend

#run unit tests
python-tests:
	echo "Running python unit tests..."
	python3 back-end/tests.py

# run postman tests
postman-tests:
	echo "Running postman tests"
	python3 back-end/postman_tests.py

# get git status
status:
	make clean
	@echo
	git branch
	git remote -v
	git status

# download files from the IDB code repo
pull:
	make clean
	@echo
	git pull
	git status


